package de.uniks.pmg.android5.gamebox.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.uniks.pmg.android5.gamebox.R;
import de.uniks.pmg.android5.gamebox.model.Game;
import de.uniks.pmg.android5.gamebox.model.Player;

/**
 *  Handles Notifications and Alarm for GameBox.
 *  Alarm time is drawn from String Resources (pref_alarm_hour, pref_alarm_minute)
 */

public class NotificationPublisher extends BroadcastReceiver{

    //private static String hour;
    //private static String minutes;
    private static int hour;
    private static int minutes;

    public static String NOTIFICATION_ID = "notification-id";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        //Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        Notification n = buildNotification(context);
        notificationManager.notify(id, n);
    }

    //method for building GameBox notifications.
    static Notification buildNotification(Context context) {
        // Get the player from the model to get the games and statistics
        Player player = GameBoxUtils.loadPlayerFromJson(context);

        // Get the game with the lowest winrate (or one that has not been played yet)
        Game game = GameBoxUtils.getGameWithLowOrNoWinrate(context);

        // Get the name of the game
        String gameName = GameBoxUtils.getGameTitleForGame(context, game);

        // Build a Notification here by setting up title, sound, icon and the content
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.channel_id));
        builder.setSmallIcon(R.drawable.ic_notifications_black_24dp);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setAutoCancel(true);
        builder.setContentTitle(context.getString(R.string.notification_title));
        builder.setContentText(context.getString(R.string.notification_text, gameName));

        // Determine which Activity to start
        Class<? extends Activity> activityToStart = GameBoxUtils.getActivityOfGame(game);

        // Create the intent
        Intent intentToStart = new Intent(context, activityToStart);

        // Add the player object, because the activities need it
        intentToStart.putExtra("PLAYER", player);

        // Add a back stack, so that finishing the Activity results in a 'normal' behaviour
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(activityToStart);
        stackBuilder.addNextIntent(intentToStart);

        // Set ContentIntent to the intent the stackBuilder gives us
        builder.setContentIntent(stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT));
        return builder.build();
    }


    //needs to be the same pendingIntent (requestcode and intent) to successfully cancel it
    static PendingIntent buildPendingIntent(Context context){

        Intent intent = new Intent(context, NotificationPublisher.class);
        intent.putExtra(NOTIFICATION_ID, 1);

        return PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
    }

    static void setAlarm(Context context) {

        //set up a calendar with the time of the day the alarm clock is supposed to fire
        Calendar calendar = Calendar.getInstance();

        // Get default values for hours and minutes from string resource
        String defaultTime = context.getString(R.string.pref_default_notification_alarm_time);
        Calendar cal = Calendar.getInstance();
        int defaultHour;
        int defaultMinutes;
        try {
            // We assume that the default value is in 24h format like hh:mm.
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            cal.setTime(sdf.parse(defaultTime));
            defaultHour = cal.get(Calendar.HOUR_OF_DAY);
            defaultMinutes = cal.get(Calendar.MINUTE);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        // Get hours and minutes from SharedPreferences.
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        hour = sharedPreferences.getInt(context.getString(R.string.pref_key_alarm_hour_notification), defaultHour);
        minutes = sharedPreferences.getInt(context.getString(R.string.pref_key_alarm_minutes_notification), defaultMinutes);


        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, 0);
        //add the notification task in form of a PendingIntent to the AlarmManager
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                .setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY,
                        buildPendingIntent(context)
                );
    }

    public static void enableAlarmClock(Context context) {
        setAlarm(context);
        //Enables the BootReceiver, which sets the alarm after a reboot
        ComponentName componentName = new ComponentName(context, BootReceiver.class);
        PackageManager packageManager = context.getPackageManager();
        packageManager.setComponentEnabledSetting(componentName,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }

    public static void disableAlarmClock(Context context) {

        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                .cancel(buildPendingIntent(context));
        //Disables the BootReceiver, which sets the alarm after a reboot
        ComponentName componentName = new ComponentName(context, BootReceiver.class);
        PackageManager packageManager = context.getPackageManager();
        packageManager.setComponentEnabledSetting(componentName,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }

    //push a Notification for test
    public static void testNotification(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 1);
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                .setExact(
                        AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        buildPendingIntent(context)
                );
    }
}
