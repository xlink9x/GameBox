package de.uniks.pmg.android5.gamebox.model;

import java.io.Serializable;
import java.util.Arrays;

import de.uniks.pmg.android5.gamebox.R;

/**
 * Created by link9 on 21.12.17.
 */

public abstract class Game implements Serializable {

    @Deprecated private int _difficulty; /** Don't use this, use Difficulty.NORMAL/HARD instead */
    private Difficulty difficulty;
    private Player player;

    /** Stats in this way: [#won, #lost, #played] **/
    private int[] statsNormal = new int[]{0, 0, 0};
    private int[] statsHard = new int[]{0, 0, 0};

    public Game() {
        //this(0);
        this(Difficulty.NORMAL);
    }

    @Deprecated public Game(int difficulty) {
        this._difficulty = difficulty;
    }

    public Game(Difficulty diff) {
        this.difficulty = diff;
    }

    abstract public String getGameName();

    /**
     * Call this method if the Game was won. This will update the stats.
     */
    public void won() {
        switch (difficulty) {
            case NORMAL:
                statsNormal[0]++;   // #won
                statsNormal[2]++;   // #played
                break;
            case HARD:
                statsHard[0]++;   // #won
                statsHard[2]++;   // #played
                break;
            default:
        }
    }

    /**
     * Call this method if the Game was lost. This will update the stats.
     */
    public void lost() {
        switch (difficulty) {
            case NORMAL:
                statsNormal[1]++;   // #lost
                statsNormal[2]++;   // #played
                break;
            case HARD:
                statsHard[1]++;   // #lost
                statsHard[2]++;   // #played
                break;
            default:
        }
    }

    /**
     * Call this method if the Game ended in a draw. This will update the stats.
     */
    public void draw() {
        switch (difficulty) {
            case NORMAL:
                statsNormal[2]++;   // #played
                break;
            case HARD:
                statsHard[2]++;   // #played
                break;
            default:
        }
    }

    // ***********************************************************
    // Getters and Setters
    // ***********************************************************

    /**
     * Calculates the winning rate for this Game. This is done by the following formula:<br>
     * #wins / #plays<br>
     * Note that this method will return the rate as a decimal number between 0.0 and 1.0, both
     * including.
     * @param difficulty The {@link Difficulty} for which the winning rate will be calculated.
     * @return A double between 0.0 and 1.0, both including, indicating the winning rate for this
     * game. 0.0 means a winning rate of 0% for this game, 1.0 means all games has been won (100%).
     */
    public double getWinRate(Difficulty difficulty) {
        double winRate;

        double wins = difficulty == Difficulty.NORMAL ? statsNormal[0] : statsHard[0];
        double plays = difficulty == Difficulty.NORMAL ? statsNormal[2] : statsHard[2];

        winRate = wins / plays;
        if(Double.isNaN(winRate)) {
            winRate = 0.0;
        }
        return winRate;
    }

    public int get_difficulty() {
        return _difficulty;
    }

    public void set_difficulty(int _difficulty) {
        this._difficulty = _difficulty;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public int[] getStatsNormal() {
        return statsNormal;
    }

    public void setStatsNormal(int[] statsNormal) {
        this.statsNormal = statsNormal;
    }

    public int[] getStatsHard() {
        return statsHard;
    }

    public void setStatsHard(int[] statsHard) {
        this.statsHard = statsHard;
    }

    public void resetStats(Difficulty difficulty) {

        switch (difficulty) {
            case NORMAL:
                Arrays.fill(statsNormal, 0);
                break;
            case HARD:
                Arrays.fill(statsHard, 0);
                break;
            default: break;
        }

    }

    public enum Difficulty {
        NORMAL, HARD
    }
}
