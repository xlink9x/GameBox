package de.uniks.pmg.android5.gamebox.model;

import static de.uniks.pmg.android5.gamebox.model.Game.Difficulty.NORMAL;

/**
 * Created by link9 on 21.12.17.
 */

public class ReactionGame extends Game {
    public static final String GAME_NAME = "reaction";

    public static final int AMOUNT_SQUARES_NORMAL = 9;
    public static final int AMOUNT_STEPS_NORMAL = 4;
    public static final int TIME_TO_REACT_NORMAL = 3000;

    public static final int AMOUNT_SQUARES_HARD = 16;
    public static final int AMOUNT_STEPS_HARD = 7;
    public static final int TIME_TO_REACT_HARD = 1500;

    private int amountSquares;
    private int amountSteps;
    private int duration;

    private int stepsDone;

    public ReactionGame() {
        this(NORMAL);
    }

    public ReactionGame(Difficulty difficulty) {
        super(difficulty);

        switch (difficulty) {
            case NORMAL:
                amountSquares = AMOUNT_SQUARES_NORMAL;
                amountSteps = AMOUNT_STEPS_NORMAL;
                duration = TIME_TO_REACT_NORMAL;
                break;
            case HARD:
                amountSquares = AMOUNT_SQUARES_HARD;
                amountSteps = AMOUNT_STEPS_HARD;
                duration = TIME_TO_REACT_HARD;
        }
        stepsDone = 0;
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    public void increaseStepsDone() {
        stepsDone++;
    }

    public void resetGame() { stepsDone = 0; }

    // Getters and Setters

    public int getAmountSquares() {
        return amountSquares;
    }

    public int getAmountSteps() {
        return amountSteps;
    }

    public int getDuration() {
        return duration;
    }

    public int getStepsDone() {
        return stepsDone;
    }
}
