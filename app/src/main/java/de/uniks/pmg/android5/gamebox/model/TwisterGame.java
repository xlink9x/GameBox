package de.uniks.pmg.android5.gamebox.model;

/**
 * Created by link9 on 21.12.17.
 */

public class TwisterGame extends Game {
    public static final String GAME_NAME = "twister";

    private static final int DEFAULT_DURATION = 10;
    private static final int DEFAULT_TURNS = 4;
    private static final int HARD_DURATION = 12;
    private static final int HARD_TURNS = 8;

    private int duration;
    private int turns;

    public TwisterGame() {
        this(Difficulty.NORMAL);
    }

    public TwisterGame(Difficulty difficulty) {
        super(difficulty);

        setDifficulty(difficulty);
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        super.setDifficulty(difficulty);

        switch (difficulty) {
            case NORMAL:
                duration = DEFAULT_DURATION;
                turns = DEFAULT_TURNS;
                break;
            case HARD:
                duration = HARD_DURATION;
                turns = HARD_TURNS;
                break;
            default:break;
        }
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTurns() {
        return turns;
    }

    public void setTurns(int turns) {
        this.turns = turns;
    }
}
