package de.uniks.pmg.android5.gamebox;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GameListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GameListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GameListFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public GameListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GameListFragment.
     */
    public static GameListFragment newInstance() {
        GameListFragment fragment = new GameListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_list, container, false);

        // Create the buttons handlers (call onFragmentInteraction in MainActivity through the
        // interface)
        view.findViewById(R.id.button_play_simon).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_play_colors).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_play_reaction).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_play_tictactoe).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_play_twister).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_play_number).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_simon).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_colors).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_reaction).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_tictactoe).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_twister).setOnClickListener(this::onButtonPressed);
        view.findViewById(R.id.button_rules_number).setOnClickListener(this::onButtonPressed);

        return view;
    }

    public void onButtonPressed(View view) {
        if (mListener != null) {
            mListener.onFragmentInteraction(view);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(View view);
    }
}
