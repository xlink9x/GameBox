package de.uniks.pmg.android5.gamebox.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.graph.GraphAdapterBuilder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import de.uniks.pmg.android5.gamebox.ColorGameActivity;
import de.uniks.pmg.android5.gamebox.NumberGameActivity;
import de.uniks.pmg.android5.gamebox.R;
import de.uniks.pmg.android5.gamebox.ReactionGameActivity;
import de.uniks.pmg.android5.gamebox.SimonGameActivity;
import de.uniks.pmg.android5.gamebox.TicTacToeGameActivity;
import de.uniks.pmg.android5.gamebox.TwisterGameActivity;
import de.uniks.pmg.android5.gamebox.model.ColorGame;
import de.uniks.pmg.android5.gamebox.model.Game;
import de.uniks.pmg.android5.gamebox.model.NumberGame;
import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.ReactionGame;
import de.uniks.pmg.android5.gamebox.model.SimonGame;
import de.uniks.pmg.android5.gamebox.model.TicTacToeGame;
import de.uniks.pmg.android5.gamebox.model.TwisterGame;

/**
 * This class provides some helper methods which probably need to be called
 * more than once.
 */

public class GameBoxUtils {

    public static final String FILENAME_PLAYER = "player.json";

    /**
     * Saves the given {@link Player} to a json file. It can then be retrieved by calling
     * {@link GameBoxUtils#loadPlayerFromJson(Context)}.
     * @param context The {@link Context} from which the FileOutputStream will open the file to
     *                write to it, usually the current {@link android.app.Activity}.
     * @param player The {@link Player} object to save.
     */
    public static void savePlayerToJson(Context context, Player player) {
        try {
            FileOutputStream outputStream;
            outputStream = context.openFileOutput(FILENAME_PLAYER, Context.MODE_PRIVATE);

            // Get a json string from player
            GsonBuilder gsonBuilder = new GsonBuilder();
            new GraphAdapterBuilder()
                    .addType(Player.class)
                    .registerOn(gsonBuilder);
            gsonBuilder.registerTypeAdapter(Game.class, new GameClassAdapter());
            Gson gson = gsonBuilder.create();

            String json = gson.toJson(player);

            // Save to file
            outputStream.write(json.getBytes());

        } catch (IOException e) {
            // Something went wrong, that's bad :(
            Log.e("FileError", "Error saving \"player.json\"...");
            e.printStackTrace();
        }
    }

    /**
     * Loads a {@link Player} object from a json file and returns it. If the file does not exist or
     * the player object could not be retrieved from it, it will return null.
     * @param context The {@link Context} from which the FileInputStream will open the file, usually
     *                the current {@link android.app.Activity}.
     * @return A {@link Player} object on success, null otherwise.
     */
    public static Player loadPlayerFromJson(Context context) {
        Player player = null;
        try {
            // Get the input stream
            FileInputStream inputStream = context.openFileInput(FILENAME_PLAYER);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            // Build a Gson object to load from json.
            GsonBuilder gsonBuilder = new GsonBuilder();
            new GraphAdapterBuilder()
                    .addType(Player.class)
                    .registerOn(gsonBuilder);
            // Add an Adapter for the abstract Game class
            gsonBuilder.registerTypeAdapter(Game.class, new GameClassAdapter());
            Gson gson = gsonBuilder.create();

            player = gson.fromJson(reader, Player.class);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return player;
    }



    /**
     * Converts the values from the given vector to degrees. The values in this vector must be in
     * radians. This vector probably comes from
     * {@link android.hardware.SensorManager#getOrientation(float[], float[])}
     * @param vector The vector with values in radians.
     */
    public static void convertToDegrees(float[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i]<0)
            {
                vector[i] = vector[i]*(-1);
                vector[i] = 359-(float)Math.toDegrees(vector[i]);
            }else vector[i] = (float)Math.toDegrees(vector[i]);
        }
    }

    /**
     * Returns an {@link IdRes} of an image preview for the given game.
     * @param game The game object.
     * @return The {@link IdRes} of the image for the game.
     */
    public static int getImagePreviewForGame(Game game) {
        if(game instanceof SimonGame) return R.drawable.simon_preview;
        if(game instanceof ColorGame) return R.drawable.colors_preview;
        if(game instanceof ReactionGame) return R.drawable.reactions_preview;
        if(game instanceof TicTacToeGame) return R.drawable.tictactoe_preview;
        if(game instanceof TwisterGame) return R.drawable.twister_preview1;
        if(game instanceof NumberGame) return R.drawable.numbers_preview1;
        return 0;
    }

    /**
     * Returns the name of the given game as it is defined in strings.xml.
     * @param context The context within the UI to get the resources from.
     * @param game The game to get the name for.
     * @return The name for the given Game as it is defined in strings.xml.
     */
    public static String getGameTitleForGame(Context context, Game game) {
        return context.getResources().getString(context.getResources()
                .getIdentifier("game_title_" + game.getGameName(),
                        "string", context.getPackageName()));
    }

    /**
     * Creates and shows a dialog to the user. This dialog is an {@link AlertDialog} that has
     * two button (ok, cancel), a title and a message. Pass method references to this method that
     * needs to be called in each case (onPositive: ok, onNegative: cancel, onCancel: back-button).
     * If you do not want anything to happen, pass null.
     * <p>
     *     Sample Usage (from {@link de.uniks.pmg.android5.gamebox.SimonGameActivity}:<br/><br/>
     *     {@code GameBoxUtils.showAlertDialog(this, R.string.title, R.string.message,
     *     this::restartGame, this::finish, this::finish);}
     * </p>
     * @param context The {@link Context} in which this Dialog is shown. Usually the current
     *                {@link android.app.Activity}
     * @param titleRes The {@link StringRes} of the title for this dialog.
     * @param messageRes The {@link StringRes} of the message for this dialog.
     * @param onPositive The method reference to be called if the ok-button was clicked.
     * @param onNegative The method reference to be called if the cancel-button was clicked.
     * @param onCancel The method reference to be called if the user uses the hardware back-button.
     */
    public static void showAlertDialog(Context context, @StringRes int titleRes,
                                       @StringRes int messageRes, Runnable onPositive,
                                       Runnable onNegative, Runnable onCancel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder .setMessage(messageRes)
                .setTitle(titleRes);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(onNegative != null)
                    onNegative.run();
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(onPositive != null)
                    onPositive.run();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(onCancel != null)
                onCancel.run();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows a dialog whether you are sure to reset your statistics
     *
     * @param context The {@link Context} in which this Dialog is shown. Usually the current
     *                {@link android.app.Activity}
     * @param game The Game in which the statistics should be reset
     * @param difficulty The difficulty of the game, which will be reset
     * @param mPlayer The player from whom the changes will be saved
     */
    public static void showStatAlertDialog(Context context, Game game, Game.Difficulty difficulty, Player mPlayer ) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder .setMessage(R.string.stat_reset_msg_sure)
                .setTitle(getGameTitleForGame(context, game));

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                game.resetStats(difficulty);
                GameBoxUtils.savePlayerToJson(context, mPlayer);
                dialog.dismiss();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    /**
     * Determines and returns the {@link Game} with the lowest winrate or the game, that has not
     * been played yet (which is prioritized over a 0% winrate of an already played game).
     * @param context A {@link Context} to be able to load the Player from json.
     * @return The {@link Game} with the lowest winrate or a Game that has not been played yet.
     */
    public static Game getGameWithLowOrNoWinrate(Context context) {
        Player player = loadPlayerFromJson(context);
        Game[] games = player.getGames();
        Game lowestGame = games[0];
        for (Game g: games) {
            // Game has not been played
            if(g.getStatsNormal()[2] == 0) {
                return g;
            }

            double gWinrate = g.getWinRate(Game.Difficulty.NORMAL);
            double lowestGameWinrate = lowestGame.getWinRate(Game.Difficulty.NORMAL);
            if (gWinrate < lowestGameWinrate)
            {
                lowestGame = g;
            }

        }
        return lowestGame;
    }

    /**
     * Links all the {@link Game}s to its appropriate {@link Activity}. It returns the Activity for
     * the given Game.
     * @param game The {@link Game} for which you want to have the {@link Activity}.
     * @return The {@link Activity} for the given {@link Game}.
     */
    public static Class<? extends Activity> getActivityOfGame(Game game) {
        if (game instanceof SimonGame)
            return SimonGameActivity.class;
        if (game instanceof ColorGame)
            return ColorGameActivity.class;
        if (game instanceof  ReactionGame)
            return ReactionGameActivity.class;
        if (game instanceof TicTacToeGame)
            return TicTacToeGameActivity.class;
        if (game instanceof TwisterGame)
            return TwisterGameActivity.class;
        if (game instanceof NumberGame)
            return NumberGameActivity.class;
        return null;
    }

    /**
     * Shows an Alert with all Devs
     * @param context A {@link Context} to be able to build the Dialog
     */
    public static void showDevAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder .setMessage(R.string.msg_imprint)
                .setTitle(R.string.title_imprint);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
