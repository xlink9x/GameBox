package de.uniks.pmg.android5.gamebox.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by link9 on 21.12.17.
 */

public class Player implements Serializable {

    /** Name of the player */
    private String name;

    /** Games */
    private Game[] games;

    public Player() {
        this("");
    }

    public Player(String name) {
        this.name = name;

        SimonGame simonGame = new SimonGame();
        ColorGame colorGame = new ColorGame();
        ReactionGame reactionGame = new ReactionGame();
        TicTacToeGame ticTacToeGame = new TicTacToeGame();
        TwisterGame twisterGame = new TwisterGame();
        NumberGame numberGame = new NumberGame();

        simonGame.setPlayer(this);
        colorGame.setPlayer(this);
        reactionGame.setPlayer(this);
        ticTacToeGame.setPlayer(this);
        twisterGame.setPlayer(this);
        numberGame.setPlayer(this);

        this.games = new Game[] {simonGame, colorGame, reactionGame, ticTacToeGame, twisterGame, numberGame};

    }


    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", games=" + Arrays.toString(games) +
                '}';
    }

    // ***********************************************************
    // Getters and Setters
    // ***********************************************************
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game[] getGames() {
        return games;
    }

    public SimonGame getSimonGame() {
        return (SimonGame) games[Games.SimonGame.ordinal()];
    }

    public void setSimonGame(SimonGame simonGame) {
        this.games[Games.SimonGame.ordinal()] = simonGame;
    }

    public ColorGame getColorGame() {
        return (ColorGame) games[Games.ColorGame.ordinal()];
    }

    public void setColorGame(ColorGame colorGame) {
        this.games[Games.ColorGame.ordinal()] = colorGame;
    }

    public ReactionGame getReactionGame() {
        return (ReactionGame) this.games[Games.ReactionsGame.ordinal()];
    }

    public void setReactionGame(ReactionGame reactionGame) {
        this.games[Games.ReactionsGame.ordinal()] = reactionGame;
    }

    public TicTacToeGame getTicTacToeGame() {
        return (TicTacToeGame) this.games[Games.TicTacToeGame.ordinal()];
    }

    public void setTicTacToeGame(TicTacToeGame ticTacToeGame) {
        this.games[Games.TicTacToeGame.ordinal()] = ticTacToeGame;
    }

    public TwisterGame getTwisterGame() {
        return (TwisterGame) this.games[Games.TwisterGame.ordinal()];
    }

    public void setTwisterGame(TwisterGame twisterGame) {
        this.games[Games.TwisterGame.ordinal()] = twisterGame;
    }

    public NumberGame getNumberGame() {
        return (NumberGame) this.games[Games.NumberGame.ordinal()];
    }

    public void setNumberGame(NumberGame numberGame) {
        this.games[Games.NumberGame.ordinal()] = numberGame;
    }

    private enum Games {
        SimonGame, ColorGame, ReactionsGame, TicTacToeGame, TwisterGame, NumberGame
    }
}
