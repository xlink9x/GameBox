package de.uniks.pmg.android5.gamebox;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

import de.uniks.pmg.android5.gamebox.model.NumberGame;
import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class NumberGameActivity extends AppCompatActivity {

    // Views
    private TextView mTextviewCurrentNumber;
    private TextView mTextviewText;
    private TextView mTextviewLowestValue;
    private TextView mTextviewHighestValue;
    private TextView mTextviewLastNumber;

    // Current GameState
    private GameState mCurrentGameState;

    private Player mPlayer;
    private NumberGame mNumberGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_game);
        getSupportActionBar().setTitle(R.string.game_title_number);

        // Load Views
        mTextviewCurrentNumber = findViewById(R.id.textview_current_number);
        mTextviewText = findViewById(R.id.textview_numbers_text);
        mTextviewLowestValue = findViewById(R.id.textview_lowest_value);
        mTextviewHighestValue = findViewById(R.id.textview_highest_value);
        mTextviewLastNumber = findViewById(R.id.textview_numbers_last_number);

        // Get the player and game
        mPlayer = (Player) getIntent().getSerializableExtra("PLAYER");
        mNumberGame = mPlayer.getNumberGame();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startGame();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mNumberGame.resetGame();

        // If Player is still playing, count the current game as a loss
        if(mCurrentGameState == GameState.STATE_PLAYING) {
            mNumberGame.lost();
        }

        // Save the model
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }

    /**
     * Starts the game by going into the right GameState, reset the game in the model, creating
     * a random number between lowestValue and highestValue and reset all the Views.
     */
    private void startGame() {
        // Change GameState to STATE_PLAYING, so if user leaves the activity, it counts as a loss
        mCurrentGameState = GameState.STATE_PLAYING;

        // Resets the game in the model, so we can start from fresh
        mNumberGame.resetGame();

        // Create a random number between lowestValue and highestValue
        Random random = new Random();

        mNumberGame.setLowestValue(random.nextInt(NumberGame.MAX_LOWEST_VALUE));
        mNumberGame.setHighestValue(mNumberGame.getLowestValue() + mNumberGame.getNumberRange());

        int number = random.nextInt(mNumberGame.getNumberRange()) + mNumberGame.getLowestValue();
        mNumberGame.setNumberToGuess(number);

        // Reset TextViews
        mTextviewText.setText(getString(R.string.numbers_start_text,
                mNumberGame.getLowestValue(), mNumberGame.getHighestValue()));
        mTextviewLowestValue.setText(getString(R.string.number_lowest_value, mNumberGame.getLowestValue()));
        mTextviewHighestValue.setText(getString(R.string.number_highest_value,mNumberGame.getHighestValue()));
        mTextviewCurrentNumber.setText(getString(R.string.number_game_0));
        mTextviewLastNumber.setText("");

    }

    /**
     * Method that gets called when one of the numbers of the dialer-like button grid gets clicked.
     * It adds the value to mTextviewCurrentNumber, from where the number gets parsed later.
     * @param view
     */
    public void onNumberClicked(View view) {
        // The View is actually a button
        Button button = (Button) view;

        // Get the value of the button, so that we can add it to the mTextviewCurrentNumber.
        CharSequence value = button.getText();

        // We need to check if the previous value was a 0. If so, replace it with the current value
        String oldString = mTextviewCurrentNumber.getText().toString();
        if(oldString.equals("0")) oldString = "";

        // Add value to the TextView
        mTextviewCurrentNumber.setText(oldString + value);
    }

    /**
     * Method that gets called when the delete button of the dialer-like button grid gets clicked.
     * It just deletes the contents of the mTextviewCurrentNumber and sets it back to 0.
     * @param view
     */
    public void onDeleteClicked(View view) {
        mTextviewCurrentNumber.setText(getString(R.string.number_game_0));
    }

    /**
     * Method that gets called when the enter button of the dialer-like button grid gets clicked.
     * The magic happens here: We check if the user may has won, and if not, it shows a message
     * in mTextviewText.
     * @param view
     */
    public void onEnterClicked(View view) {
        int n = Integer.parseInt(mTextviewCurrentNumber.getText().toString());

        // Reset the mTextviewCurrentNumber
        mTextviewCurrentNumber.setText(getString(R.string.number_game_0));

        // See if number is actually in range
        if(n < mNumberGame.getLowestValue() || n > mNumberGame.getHighestValue()) {
            mTextviewText.setText(R.string.number_invalid_number);
            return;
        }

        // Update the last number typed in
        mTextviewLastNumber.setText(getString(R.string.number_last_number, n));

        // Check if user won
        if(n == mNumberGame.getNumberToGuess()) {
            mTextviewText.setText(getString(R.string.number_won, n));

            // User won
            onUserWon();

            return;
        }

        // Determine which message to show (too small, too big)
        if(n < mNumberGame.getNumberToGuess()) {
            mTextviewText.setText(R.string.number_too_small);
        } else if (n > mNumberGame.getNumberToGuess()) {
            mTextviewText.setText(R.string.number_too_big);
        }

        // Increase the current turn
        mNumberGame.increaseCurrentTurn();

        // If user has no more turns, he has lost
        if(mNumberGame.getCurrentturn() > mNumberGame.getMaxTurns()) {
            mTextviewText.setText(getString(R.string.number_lost, n));

            // user lost
            onUserLost();
        }

    }

    /**
     * Call this when the user has won the game. We update the GameState, update the model and
     * show a dialog message if the user wants to retry.
     */
    private void onUserWon() {
        // Update the GameState
        mCurrentGameState = GameState.STATE_ENDED;

        // User won
        mNumberGame.won();

        GameBoxUtils.showAlertDialog(this, R.string.game_title_number,
                R.string.dialog_message_won,
                this::restartGame,
                this::finish,
                this::finish);
    }

    /**
     * Call this when the user has lost the game. We update the GameState, update the model and
     * show a dialog message if the user wants to retry.
     */
    private void onUserLost() {
        // Update the GameState
        mCurrentGameState = GameState.STATE_ENDED;

        // User lost
        mNumberGame.lost();

        GameBoxUtils.showAlertDialog(this, R.string.game_title_number,
                R.string.dialog_message_lost,
                this::restartGame,
                this::finish,
                this::finish);
    }

    /**
     * Restarting the Game by simply starting it again. Nothing fancy...
     */
    private void restartGame() {
        startGame();
    }

    /**
     * The States of this game.<br>
     * <b>STATE_PLAYING</b>: The Game runs and the user can type in his numbers to guess.<br>
     * <b>STATE_ENDED</b>: The Game has ended and the user gets asked if he wants to try again.
     */
    private enum GameState {
        STATE_PLAYING, STATE_ENDED
    }
}
