package de.uniks.pmg.android5.gamebox.model;

/**
 * Created by link9 on 02.02.18.
 */

public class NumberGame extends Game {
    public static final String GAME_NAME = "number";
    public static final int NUMBER_RANGE_NORMAL = 10;
    public static final int NUMBER_RANGE_HARD = 20;
    public static final double MAX_TURN_SCALAR_NORMAL = 1;
    public static final double MAX_TURN_SCALAR_HARD = 0.75;
    public static final int LOWEST_VALUE_DEFAULT_NORMAL = 0;
    public static final int LOWEST_VALUE_DEFAULT_HARD = 0;
    public static final int HIGHEST_VALUE_DEFAULT_NORMAL = 10;
    public static final int HIGHEST_VALUE_DEFAULT_HARD = 20;
    public static final int MAX_LOWEST_VALUE = 100;


    private int numberToGuess;
    private transient int currentTurn;

    private int maxTurns;
    private int numberRange;
    private int lowestValue;
    private int highestValue;

    public NumberGame() {
        this(Difficulty.NORMAL);
    }

    public NumberGame(Difficulty diff) {
        super(diff);
        setDifficulty(diff);
        currentTurn = 0;
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        super.setDifficulty(difficulty);
        switch(difficulty) {
            case NORMAL:
                numberRange = NUMBER_RANGE_NORMAL;
                //Max needed turn when using binary search
                maxTurns = (int) (Math.ceil(Math.log(numberRange + 1)/Math.log(2)) * MAX_TURN_SCALAR_NORMAL);
                lowestValue = LOWEST_VALUE_DEFAULT_NORMAL;
                highestValue = HIGHEST_VALUE_DEFAULT_NORMAL;
                break;
            case HARD:
                numberRange = NUMBER_RANGE_HARD;
                //Max needed turn when using binary search, but a little less so that
                //you dont always easily win
                maxTurns = (int) (Math.ceil(Math.log(numberRange + 1)/Math.log(2)) * MAX_TURN_SCALAR_HARD);
                lowestValue = LOWEST_VALUE_DEFAULT_HARD;
                highestValue = HIGHEST_VALUE_DEFAULT_HARD;
                break;
            default:
                break;
        }
    }

    public void resetGame() {
        currentTurn = 0;
    }

    public void increaseCurrentTurn() {
        currentTurn++;
    }

    //=========================


    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    public int getNumberToGuess() {
        return numberToGuess;
    }

    public void setNumberToGuess(int numberToGuess) {
        this.numberToGuess = numberToGuess;
    }

    public int getCurrentturn() {
        return currentTurn;
    }

    public int getMaxTurns() {
        return maxTurns;
    }

    public int getNumberRange() {
        return numberRange;
    }

    public int getLowestValue() {
        return lowestValue;
    }

    public void setLowestValue(int lowestValue) {
        this.lowestValue = lowestValue;
    }

    public int getHighestValue() {
        return highestValue;
    }

    public void setHighestValue(int highestValue) {
        this.highestValue = highestValue;
    }
}
