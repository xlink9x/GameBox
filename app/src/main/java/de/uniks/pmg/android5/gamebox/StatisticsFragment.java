package de.uniks.pmg.android5.gamebox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

import de.uniks.pmg.android5.gamebox.model.Game;
import de.uniks.pmg.android5.gamebox.model.Player;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StatisticsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StatisticsFragment extends Fragment {

    private Switch mSwitch;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Player mPlayer; // The player object to get the stats from

    public StatisticsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StatisticsFragment.
     */
    public static StatisticsFragment newInstance(Player player) {
        StatisticsFragment fragment = new StatisticsFragment();
        Bundle args = new Bundle();
        args.putSerializable("PLAYER", player);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);

        // Load the Player from arguments
        if (getArguments() != null) {
            mPlayer = (Player) getArguments().getSerializable("PLAYER");
        }

        mSwitch = view.findViewById(R.id.switch_stats_hard_mode);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    createAdapterFromDifficulty(Game.Difficulty.HARD);
                } else {
                    createAdapterFromDifficulty(Game.Difficulty.NORMAL);
                }
            }
        });

        // Create the RecyclerView and create an adapter for it
        mRecyclerView = view.findViewById(R.id.recyclerview_stats);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Create the adapter and set the RecyclerViews Adapter to this
        if(mSwitch.isChecked()) {
            createAdapterFromDifficulty(Game.Difficulty.HARD);
        } else {
            createAdapterFromDifficulty(Game.Difficulty.NORMAL);
        }

        return view;
    }

    /**
     * Creates a List of all Games and sets the Adapter for the RecyclerView.
     * The difficulty determines which set of statistics get retrieved from the game object.
     * @param diff The {@link de.uniks.pmg.android5.gamebox.model.Game.Difficulty} for the dataset.
     */
    private void createAdapterFromDifficulty(Game.Difficulty diff) {
        // Create the data for the Adapter (A list of all games)
        List<Game> gameList = new ArrayList<>();
        for (Game game : mPlayer.getGames()) {
            gameList.add(game);

        }

        mAdapter = new GameStatsAdapter(gameList, diff);
        mRecyclerView.setAdapter(mAdapter);
    }

}

