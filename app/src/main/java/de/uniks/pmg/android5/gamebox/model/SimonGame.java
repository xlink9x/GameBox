package de.uniks.pmg.android5.gamebox.model;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by link9 on 21.12.17.
 */

public class SimonGame extends Game {
    private static final String GAME_NAME = "simon";

    private static final int AMOUNT_FIELDS = 4;
    private static final int AMOUNT_START_TONES = 1;
    private static final int AMOUNT_START_TONES_HARD = 4;

    private static final int AMOUNT_TURNS_DEFAULT = 10;
    private static final int AMOUNT_TURNS_HARD = 20;

    private static final int DEFAULT_TIME_ON = 420;
    private static final int DEFAULT_TIME_OFF = 50;

    private int amountStartTones = AMOUNT_START_TONES;
    private int amountTurns = AMOUNT_TURNS_DEFAULT;

    private int timeOn = DEFAULT_TIME_ON;
    private int timeOff = DEFAULT_TIME_OFF;

    transient private Random random;
    transient private ArrayList<Integer> sequence;

    public SimonGame() {
        this(Difficulty.NORMAL);
    }

    public SimonGame(Difficulty difficulty) {
        super(difficulty);

        setDifficulty(difficulty);

        sequence = new ArrayList<>();
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        super.setDifficulty(difficulty);

        switch (difficulty) {
            case NORMAL:
                amountTurns = AMOUNT_TURNS_DEFAULT;
                amountStartTones = AMOUNT_START_TONES;
                break;
            case HARD:
                amountTurns = AMOUNT_TURNS_HARD;
                amountStartTones = AMOUNT_START_TONES_HARD;
                break;
        }
    }

    /**
     * Restarts the game. Resets the old sequence, updates the players
     * statistics and then starts a new game.
     */
    public void restartGame() {
        // Reset stuff
        sequence.clear();

        // Count last game as a loss
        lost();

        // start a new game
        startGame();
    }

    /**
     * This methods starts the game by creating a random sequence
     */
    public void startGame() {
        // Fill the sequence list with a few random numbers
        if(sequence == null) sequence = new ArrayList<>();
        for(int i = 0; i<amountStartTones; i++) {
            extendSequence();
        }

        // reset timeOn
        timeOn = DEFAULT_TIME_ON;

    }

    /**
     * Checks if the user did input the right value at the given index.
     * @param index The index in the sequence.
     * @param input The user's input. Value between 0 and AMOUNT_FIELDS - 1.
     * @return If the user made a mistake, it returns false, true otherwise.
     */
    public boolean checkTurn(int index, int input) {
        return sequence.get(index) == input;
    }

    /**
     * Extends the sequence by one value.
     */
    public void extendSequence() {
        random = new Random();
        sequence.add(random.nextInt(AMOUNT_FIELDS));

        // update the timings (only in hard mode)
        switch (getDifficulty()) {
            case HARD:
                /*
                timings are based on how far we are into the game. Normally, with a 32-tone
                sequence, we have the following timings:
                1-5: 420ms on, 50ms off
                6-13: 320ms on, 50ms off
                14-31: 220ms on, 50ms off.

                To make it easier, the calculation is basically like:
                currentLength < 15% of amountTurns: 420ms on, 50ms off
                currentLength > 15% and < 40%: 320ms on, 50ms off
                currentLength > 40%: 220ms on, 50ms off
                 */

                double progress = (double)(getSequenceLength() - amountStartTones) / (double)amountTurns;
                if(progress < 0.15) {
                    timeOn = 420;
                } else if (progress < 0.4) {
                    timeOn = 320;
                } else {
                    timeOn = 220;
                }


                break;
            default: break;
        }
    }

    // ***********************************************************
    // Getters and Setters
    // ***********************************************************

    public ArrayList<Integer> getSequence() {
        return sequence;
    }

    public int getSequenceLength() {
        return sequence.size();
    }

    public int getAmountTurns() {
        return amountTurns;
    }

    public void setAmountTurns(int amountTurns) {
        this.amountTurns = amountTurns;
    }

    public int getAmountStartTones() {
        return amountStartTones;
    }

    public void setAmountStartTones(int amountStartTones) {
        this.amountStartTones = amountStartTones;
    }

    public int getTimeOn() {
        return timeOn;
    }

    public void setTimeOn(int timeOn) {
        this.timeOn = timeOn;
    }

    public int getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(int timeOff) {
        this.timeOff = timeOff;
    }
}
