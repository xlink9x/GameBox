package de.uniks.pmg.android5.gamebox.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A Preference that will let the user choose a time in either 12h or 24h format, depending
 * on the time-format of the device. It uses a {@link TimePicker} to let the user select a
 * time from a clock.<br>
 * Make sure that the default value is in 24h format like hh:mm.<br>
 * In order to get the values (hours and minutes) see {@link #getHourOfDay()} and
 * {@link #getMinuteOfHour()}.
 */

public class TimePreference extends DialogPreference {
    private Calendar calendar;
    private TimePicker picker = null;

    public TimePreference(Context context) {
        this(context, null);
    }

    public TimePreference(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.dialogPreferenceStyle);
    }

    public TimePreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // Setup the buttons for the dialog
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        // Create a new Calendar to work with
        calendar = new GregorianCalendar();
    }

    @Override
    protected View onCreateDialogView() {
        // Create a new TimePicker view
        picker = new TimePicker(getContext());

        // check if the system is using 24h format, if so, set the picker to it
        picker.setIs24HourView(DateFormat.is24HourFormat(getContext()));

        return picker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);

        // bind the current data in the calendar to the one of the TimePicker
        picker.setHour(calendar.get(Calendar.HOUR_OF_DAY));
        picker.setMinute(calendar.get(Calendar.MINUTE));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        // If dialog was not cancelled
        if (positiveResult) {
            // Set the calendar to the values of the picker
            calendar.set(Calendar.HOUR_OF_DAY, picker.getHour());
            calendar.set(Calendar.MINUTE, picker.getMinute());

            // update the summary
            setSummary(getSummary());
            if (callChangeListener(calendar.getTimeInMillis())) {
                persistLong(calendar.getTimeInMillis());
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

        if (restoreValue) {
            if (defaultValue == null) {
                calendar.setTimeInMillis(getPersistedLong(System.currentTimeMillis()));
            } else {
                calendar.setTimeInMillis(Long.parseLong(getPersistedString((String) defaultValue)));
            }
        } else {
            if (defaultValue == null) {
                calendar.setTimeInMillis(System.currentTimeMillis());
            } else {
                // magic code :)
                // honestly, defaultValue is actually a string that is most likely defined in the
                // xml file. In this case we just assume that the user inputs a string that is
                // 1. in 24h format
                // 2. in the format of "hh:mm".

                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                try {
                    calendar.setTime(sdf.parse((String)defaultValue));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        if (calendar == null) {
            return null;
        }
        return DateFormat.getTimeFormat(getContext()).format(new Date(calendar.getTimeInMillis()));
    }

    /**
     * Returns the hour of the day as it is described in {@link Calendar#HOUR_OF_DAY}.
     * @return The {@link Calendar#HOUR_OF_DAY} of this preference.
     */
    public int getHourOfDay() {
        if(calendar == null) {
            return 0;
        }
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * Returns the minute as it is described in {@link Calendar#MINUTE}.
     * @return The {@link Calendar#MINUTE} of this preference.
     */
    public int getMinuteOfHour() {
        if(calendar == null) {
            return 0;
        }
        return  calendar.get(Calendar.MINUTE);
    }

}