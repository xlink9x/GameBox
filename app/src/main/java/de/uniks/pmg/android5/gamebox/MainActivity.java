package de.uniks.pmg.android5.gamebox;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class MainActivity extends AppCompatActivity implements GameListFragment.OnFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}, so when the activity gets resumed,
     * it will recreate the fragments. The advantage is, that then the statistics gets updated
     * every time.
     *
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /**
     * Always hold a player object and put it to the bundle when starting a
     * new activity.
     */
    private Player mPlayer;

    /**
     * Keeping track of the currently selected page of the ViewPager.
     */
    private int mCurrentlySelectedPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        createNotificationChannel();
    }
    @TargetApi(26)
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT < 26)
            return;
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel mNotificationChannel = new NotificationChannel(
                getString(R.string.channel_id),
                getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_HIGH
        );
        mNotificationChannel.setDescription(getString(R.string.channel_description));

        mNotificationChannel.enableLights(true);
        mNotificationChannel.setLightColor(Color.RED);
        mNotificationChannel.enableVibration(true);
        mNotificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mNotificationManager.createNotificationChannel(mNotificationChannel);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Load the player from the json-file, if it exists (that is every time after
        // the first start of the app)
        mPlayer = GameBoxUtils.loadPlayerFromJson(this);
        if(mPlayer == null) {
            // file does not exist, so create a new player
            mPlayer = new Player();

            // and save it to the json file
            GameBoxUtils.savePlayerToJson(this, mPlayer);
        }

        // Create the adapter that will return a fragment for each of the two
        // primary sections of the activity (Stats and Gamelist).
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);

        // Add a change listener to the ViewPager and update mCurrentlySelectedPage.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mCurrentlySelectedPage = position;
            }
        });

        // Shows the page that was last visited, if any.
        mViewPager.setCurrentItem(mCurrentlySelectedPage, false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(View view) {
        int rulesMessageId = 0;

        // determine which button was clicked.
        switch(view.getId()) {
            case R.id.button_play_simon:
                launchGameActivity(mPlayer.getSimonGame().getGameName());
                break;
            case R.id.button_play_colors:
                launchGameActivity(mPlayer.getColorGame().getGameName());
                break;
            case R.id.button_play_reaction:
                launchGameActivity(mPlayer.getReactionGame().getGameName());
                break;
            case R.id.button_play_tictactoe:
                launchGameActivity(mPlayer.getTicTacToeGame().getGameName());
                break;
            case R.id.button_play_twister:
                launchGameActivity(mPlayer.getTwisterGame().getGameName());
                break;
            case R.id.button_play_number:
                launchGameActivity(mPlayer.getNumberGame().getGameName());
                break;
            case R.id.button_rules_colors: if(rulesMessageId == 0) rulesMessageId = R.string.rules_colors;
            case R.id.button_rules_simon: if(rulesMessageId == 0) rulesMessageId = R.string.rules_simon;
            case R.id.button_rules_reaction: if(rulesMessageId == 0) rulesMessageId = R.string.rules_reaction;
            case R.id.button_rules_tictactoe: if(rulesMessageId == 0) rulesMessageId = R.string.rules_tictactoe;
            case R.id.button_rules_number: if(rulesMessageId == 0) rulesMessageId = R.string.rules_number;
            case R.id.button_rules_twister:
                if(rulesMessageId == 0) rulesMessageId = R.string.rules_twister;

                // Show message
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder .setTitle(R.string.title_rules_dialog)
                        .setMessage(rulesMessageId)
                        .setPositiveButton(R.string.dialog_button_gotit, null);

                AlertDialog dialog = builder.create();
                dialog.show();

                return;
            default:
                break;
        }
    }

    public void launchGameActivity(String gameName) {
        Bundle args = new Bundle();
        args.putSerializable("PLAYER", mPlayer);

        Intent intent = new Intent();

        if (gameName.equals(mPlayer.getSimonGame().getGameName()))
            intent.setClass(this, SimonGameActivity.class);
        else if (gameName.equals(mPlayer.getTwisterGame().getGameName()))
            intent.setClass(this, TwisterGameActivity.class);
        else if (gameName.equals(mPlayer.getNumberGame().getGameName()))
            intent.setClass(this, NumberGameActivity.class);
        else if (gameName.equals(mPlayer.getReactionGame().getGameName()))
            intent.setClass(this, ReactionGameActivity.class);
        else if (gameName.equals(mPlayer.getTicTacToeGame().getGameName()))
            intent.setClass(this, TicTacToeGameActivity.class);
        else if (gameName.equals(mPlayer.getColorGame().getGameName()))
            intent.setClass(this, ColorGameActivity.class);

        intent.putExtras(args);
        startActivity(intent);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0: // Statistiken
                    return StatisticsFragment.newInstance(mPlayer);
                case 1: // Spieleauswahl
                    return GameListFragment.newInstance();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
