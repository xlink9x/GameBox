package de.uniks.pmg.android5.gamebox;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.ReactionGame;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class ReactionGameActivity extends AppCompatActivity {

    public static final int GAME_STATE_LOST = 0;
    public static final int GAME_STATE_WON = 1;
    public static final int BLINK_ANIMATION_MS = 500;
    public static final int BLINK_ANIMATION_AMOUNT = 1;

    private Player mPlayer;
    private ReactionGame mReactionGame;

    private GridLayout squaresGrid;
    private TextView textView;
    private List<Button> squares;

    private Timer timer;
    private TimerTask timerTask;
    private Random random;
    private GameState mCurrGameState;

    private Button blinkingButton;


    /*
     * When the timer is up, the game is lost.
     */
    class LoseTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(() -> {
                mCurrGameState = GameState.GAME_STATE_LOST;
                endGame();
            });
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reaction_game);
        getSupportActionBar().setTitle(R.string.game_title_reaction);

        mCurrGameState = GameState.GAME_STATE_WAITING_FOR_START;

        squaresGrid = findViewById(R.id.squareGrid);
        textView = findViewById(R.id.reactInstruct);

        mPlayer = (Player) getIntent().getSerializableExtra("PLAYER");
        mReactionGame = mPlayer.getReactionGame();
        squares = new ArrayList<>();
        timer = new Timer();
        random = new Random();

        /*
        * Setting up the board in a square fashion:
        * 1. Specify the number_preview of rows/cols needed and set for squaresGrid
        * 2. Allocate weight for each Button to distribute the space for cells evenly
        * 3. Make the buttons fill the cells
        */
        int n = (int) Math.ceil(Math.sqrt(mReactionGame.getAmountSquares()));
        squaresGrid.setRowCount(n);
        squaresGrid.setColumnCount(n);
        /*
         * The game logic is handled inside the buttons.
         * it starts when a random button is clicked.
         * when the correct button is clicked in time, the current
         * scheduled task is stopped.
         * a new button is determined to be the button to click and a new
         * alarm is set.
         * when the determined amount of clicks is reached, the game is won.
         */
        for (int i = 0; i < mReactionGame.getAmountSquares(); i++) {
            blinkingButton = new Button(this);
            blinkingButton.setOnClickListener(view -> {
                if (mCurrGameState == GameState.GAME_STATE_WAITING_FOR_START) {
                    startCycle();
                    textView.setText(R.string.reaction_game_playing);
                    mCurrGameState = GameState.GAME_STATE_ONGOING;
                    return;
                }
                if (view != blinkingButton) {
                    return;
                }
                if (mReactionGame.getStepsDone() < mReactionGame.getAmountSteps()) {
                    mReactionGame.increaseStepsDone();
                    stopTimer();
                    startCycle();
                } else {
                    stopTimer();
                    mCurrGameState = GameState.GAME_STATE_WON;
                    endGame();
                }

            });
            GridLayout.LayoutParams param = new GridLayout.LayoutParams(
                    GridLayout.spec(GridLayout.UNDEFINED, GridLayout.CENTER, 1f),
                    GridLayout.spec(GridLayout.UNDEFINED, GridLayout.CENTER, 1f));
            param.setGravity(Gravity.FILL);
            squaresGrid.addView(blinkingButton, param);
            squares.add(blinkingButton);
        }
        blinkingButton = null;
    }


    /*
     * When the Activity is closed, the timer is disabled
     * and the player stats are saved.
     * When it is closed mid-game, the endGame() method is called to
     * clean up the game and make the player lose without
     * calling the win/lose dialog
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(mCurrGameState == GameState.GAME_STATE_ONGOING)
            endGame();
        timer.cancel();
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }

    /*
     * starts the game by starting the animation and timer
     */
    private void startCycle() {
        blinkingButton = squares.get(random.nextInt(squares.size()));
        blinkButton(blinkingButton);
        timerTask = new LoseTimerTask();
        timer.schedule(timerTask, mReactionGame.getDuration());
    }

    private void blinkButton(Button b) {
        b.startAnimation(buildAnim());
    }

    private void endGame() {
        switch (mCurrGameState) {
            case GAME_STATE_WON:
                stopTimer();

                // update the stats
                mReactionGame.won();
                mReactionGame.resetGame();

                GameBoxUtils.showAlertDialog(this, R.string.game_title_reaction,
                        R.string.dialog_message_won,
                        this::reset,
                        this::finish,
                        this::finish);
                break;
            case GAME_STATE_LOST:
                // update the stats
                mReactionGame.lost();
                mReactionGame.resetGame();
                GameBoxUtils.showAlertDialog(this, R.string.game_title_reaction,
                        R.string.dialog_message_lost,
                        this::reset,
                        this::finish,
                        this::finish);
                break;
            case GAME_STATE_ONGOING:
                //Game was probably interrupted -> make the player lose and reset
                stopTimer();
                mReactionGame.lost();
                mReactionGame.resetGame();
                reset();
                break;
            default:
                break;
        }
    }

    private void stopTimer() {
        timerTask.cancel();
        timer.purge();
    }

    private void reset() {
        mReactionGame.resetGame();
        mCurrGameState = GameState.GAME_STATE_WAITING_FOR_START;
        textView.setText(R.string.reaction_game_instruction);
    }


    /*
     * build Blinking Animation:
     * Fade from visible to invisible to visible 1 time
     */
    private AlphaAnimation buildAnim() {
        AlphaAnimation anim = new AlphaAnimation(1, 0);
        anim.setRepeatCount(BLINK_ANIMATION_AMOUNT);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatMode(Animation.REVERSE);
        anim.setDuration(BLINK_ANIMATION_MS);
        return anim;
    }

    private enum GameState {
        GAME_STATE_WAITING_FOR_START,
        GAME_STATE_ONGOING,
        GAME_STATE_WON,
        GAME_STATE_LOST
    }
}

