package de.uniks.pmg.android5.gamebox;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import java.util.List;

import de.uniks.pmg.android5.gamebox.model.ColorGame;
import de.uniks.pmg.android5.gamebox.model.Game;
import de.uniks.pmg.android5.gamebox.model.NumberGame;
import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.ReactionGame;
import de.uniks.pmg.android5.gamebox.model.SimonGame;
import de.uniks.pmg.android5.gamebox.model.TicTacToeGame;
import de.uniks.pmg.android5.gamebox.model.TwisterGame;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;
import de.uniks.pmg.android5.gamebox.util.NotificationPublisher;
import de.uniks.pmg.android5.gamebox.util.TimePreference;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list.
 *
 * Most of the following Code is repetitive. Only the first Game Fragment is getting
 * more detailed comments.
 */
public class SettingsActivity extends PreferenceActivity
{

    public Player mPlayer;

    /**
     * Creating the Activity and loading the player
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPlayer = (Player) getIntent().getSerializableExtra("PLAYER");
    }



    /**
     * This method is loading the header xml and sorting them as a list
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName)
                || SimonGamePreferenceFragment.class.getName().equals(fragmentName)
                || ColorGamePreferenceFragment.class.getName().equals(fragmentName)
                || ReactionGamePreferenceFragment.class.getName().equals(fragmentName)
                || TicTacToeGamePreferenceFragment.class.getName().equals(fragmentName)
                || TwisterGamePreferenceFragment.class.getName().equals(fragmentName)
                || NumberGamePreferenceFragment.class.getName().equals(fragmentName)
                || DeveloperPreferenceFragment.class.getName().equals(fragmentName);
    }


    /**
     * This fragment shows the first game preferences. It is used for
     * enabling or disabling the hard mode for SimonGame.
     * Furthermore it implements the preferences which resets the statics for normal/hard
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class SimonGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        SimonGame game;

        /**
         * creating the fragment and loading the player from Json to save
         * settings changes persistent.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_simon);
            setHasOptionsMenu(true);

            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getSimonGame();

            resetButtons();

        }

        /**
         * This method is used to give the reset statistics preferences their OnClickListener.
         * If clicked, the method starts an AlertDialog {@link GameBoxUtils} whether they want to reset corresponding
         * statistics.
         **/
        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        /**
         * Enabling the preference changing listener. If called: save change to the game model {@link Game}
         * and saving the changes in the model to the player in Json.
         * @param key: the key of the changed preference
         */
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_simon))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, false);
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }

            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }


    }


    /**
     * Same as SimonGame only for ColorGame
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ColorGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        ColorGame game;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_color);
            setHasOptionsMenu(true);

            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getColorGame();

            resetButtons();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_color))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, false);
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }
            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }


        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }

    }



    /**
     * Same as SimonGame only for ReactionGame
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ReactionGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        ReactionGame game;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_reaction);
            setHasOptionsMenu(true);

            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getReactionGame();

            resetButtons();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_reaction))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, false);
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }
            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }

        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }
    }



    /**
     * Same as SimonGame only for TicTacToe
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class TicTacToeGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        TicTacToeGame game;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_tictactoe);
            setHasOptionsMenu(true);


            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getTicTacToeGame();

            resetButtons();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_tictactoe))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, false);
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }
            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }

        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }

    }

    /**
     * Same as SimonGame only for TwisterGame
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class TwisterGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        TwisterGame game;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_twister);
            setHasOptionsMenu(true);


            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getTwisterGame();

            resetButtons();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            // The changed preference
            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_twister))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, Boolean.getBoolean(getString(R.string.pref_default_difficulty_twister)));
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }

            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }

        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }

    }

    /**
     * Same as SimonGame only for NumberGame
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NumberGamePreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener{

        Player mPlayer;
        NumberGame game;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_number);
            setHasOptionsMenu(true);

            mPlayer = GameBoxUtils.loadPlayerFromJson(getContext());
            game = mPlayer.getNumberGame();

            resetButtons();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            // The changed preference
            Preference pref = findPreference(key);

            if (key.equals(getString(R.string.pref_key_switch_preference_difficulty_number))) {
                Boolean difficulty = sharedPreferences.getBoolean(key, Boolean.getBoolean(getString(R.string.pref_default_difficulty_number)));
                if (!difficulty) {
                    game.setDifficulty(Game.Difficulty.NORMAL);
                }else game.setDifficulty(Game.Difficulty.HARD);
            }

            GameBoxUtils.savePlayerToJson(getContext(), mPlayer);
        }

        private void resetButtons() {
            Preference easyButton = findPreference(getString(R.string.pref_key_button_preference_easy));
            easyButton.setSummary(R.string.pref_summary_easy);
            Preference hardButton = findPreference(getString(R.string.pref_key_button_preference_hard));
            hardButton.setSummary(R.string.pref_summary_hard);

            easyButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game, Game.Difficulty.NORMAL, mPlayer );
                    return true;
                }
            });

            hardButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    GameBoxUtils.showStatAlertDialog(getContext(), game,Game.Difficulty.HARD, mPlayer );
                    return true;
                }
            });
        }

    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing the settings for the alarm clock.
     * It is quite similar to the SimonGame fragment. Only the reset button preferences are missing
     * and the switch is enabling or disabling the alert clock.
     *
     * The time getter for hour and minutes checks the format and is saving the changes to
     * the sharedPreferences. It is readable outside the SettingsActivity.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);
            setHasOptionsMenu(true);

            //setSummaries();
            setTestPreference();
        }

        private void setTestPreference() {
            Preference testButton = findPreference(getString(R.string.pref_key_button_preference_test_notification));

            testButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    NotificationPublisher.testNotification(getContext());
                    return true;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            Preference pref = findPreference(key);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (key.equals(getString(R.string.pref_key_switch_preference_notification))) {
                if(sharedPreferences.getBoolean(key, false))
                {
                    NotificationPublisher.enableAlarmClock(getContext());
                }else NotificationPublisher.disableAlarmClock(getContext());

            }

//            if (key.equals(getString(R.string.pref_key_alarm_hour_notification))) {
//                NotificationPublisher.disableAlarmClock(getContext());
//                EditTextPreference editPrefernce = (EditTextPreference) pref;
//
//                int hour = Integer.parseInt(editPrefernce.getText());
//
//                if (hour > 23)
//                {
//                    if (hour == 24)
//                    {
//                        editPrefernce.setText("00");
//                    } else {
//                        editPrefernce.setText("00");
//                        Toast.makeText(getContext(), getString(R.string.pref_wrong_format), Toast.LENGTH_LONG).show();
//                    }
//                }
//                pref.setSummary(editPrefernce.getText());
//
//
//                editor.putString(key, editPrefernce.getText());
//                editor.apply();
//                NotificationPublisher.enableAlarmClock(getContext());
//            }
//
//            if (key.equals(getString(R.string.pref_key_alarm_minutes_notification))) {
//                NotificationPublisher.disableAlarmClock(getContext());
//                EditTextPreference editPrefernce = (EditTextPreference) pref;
//
//                int minutes = Integer.parseInt(editPrefernce.getText());
//
//                if (minutes > 59)
//                {
//                    if (minutes == 60)
//                    {
//                        editPrefernce.setText("00");
//                    } else {
//                        editPrefernce.setText("00");
//                        Toast.makeText(getContext(), getString(R.string.pref_wrong_format), Toast.LENGTH_LONG).show();
//                    }
//                }
//
//                pref.setSummary(editPrefernce.getText());
//
//                editor.putString(key, editPrefernce.getText());
//                editor.apply();
//                NotificationPublisher.enableAlarmClock(getContext());
//            }

            if(key.equals(getString(R.string.pref_key_alarm_time))) {
                NotificationPublisher.disableAlarmClock(getContext());
                TimePreference timePreference = (TimePreference) pref;

                int hour = timePreference.getHourOfDay();
                int minute = timePreference.getMinuteOfHour();

                editor.putInt(getString(R.string.pref_key_alarm_hour_notification), hour);
                editor.putInt(getString(R.string.pref_key_alarm_minutes_notification), minute);

                editor.apply();
                NotificationPublisher.enableAlarmClock(getContext());
            }



        }

    }

    /**
     * The Fragment shows the Devs of this Project
     */
    public static class DeveloperPreferenceFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().onBackPressed();

            GameBoxUtils.showDevAlert(getContext());
        }
    }


}
