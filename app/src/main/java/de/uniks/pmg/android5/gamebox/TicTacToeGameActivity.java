package de.uniks.pmg.android5.gamebox;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

import java.util.ArrayList;
import java.util.List;

import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.TicTacToeGame;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class TicTacToeGameActivity extends AppCompatActivity {


    private static String EMPTY_SYMBOL ="";
    private static String PLAYER_SYMBOL ="X";
    private static String AI_SYMBOL = "O";
    private int FELDER = 0;
    private static final int EMPTY_VALUE = 0;
    private static final int PLAYER_VALUE = -1;
    private static final int AI_VALUE = 1;
    private static final int BORD_MARGIN = 7;
    private static int MINIMAX_DEPTH = 1; // 1 winnable, 2 or more = impossible
    private static final int DRAW = 3;
    private static boolean hardmode;

    private Player mPlayer;
    private TicTacToeGame mTicTacToeGame;

    private Button b[][];
    int[][] board;
    private int gameOver;
    private boolean turnAvailable = true;
    private boolean playerStarted = true;
    private GridLayout gridLayout;
    private GameState mCurrGameState;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe_game);

        mPlayer = (Player)getIntent().getSerializableExtra("PLAYER");
        mTicTacToeGame = mPlayer.getTicTacToeGame();
        hardmode = mTicTacToeGame.getDifficulty().toString().equals("HARD");


        /*
         * It would be possible to adjust the number of fields in the model.
         * But we decided to don´t be able to adjust the given parameter.
         * In this case it was the parameter #FELDER.
         *
         * I get the value of #FELDER and set them in the next step to
         * keep the parameter and use the setter. In this way we don´t get warnings ;)
         */

        FELDER = mTicTacToeGame.getSquares();
        mTicTacToeGame.setSquares(FELDER);

        board= new int[FELDER][FELDER];
        b = new Button[FELDER][FELDER];

        gridLayout = findViewById(R.id.gridLayout);
        gridLayout.setColumnCount(FELDER);
        gridLayout.setRowCount(FELDER);

        setGameBoard();

        if (hardmode) {
            MINIMAX_DEPTH = 2;
            //Get the saved board for Hard Mode
            getSavedBoard(mTicTacToeGame.getBoardHard());
        } else {
            MINIMAX_DEPTH = 1;
            getSavedBoard(mTicTacToeGame.getBoardNormal());
        }




    }

    /**
     * The method replaces the board with a saved one, if the player canceled the game
     * while it was still played.
     * @param field checking with the field and adept the changes to the board
     */
    private void getSavedBoard(int[][] field) {
        // If the saved board in model isn´t empty and has no game ending state, then fill the board of this activity
        if(!lookForUsedField(field) && lookForEmptyField(field) && !lookForWinner(field)) {
            for (int row = 0; row < FELDER; row++) {
                for (int column = 0; column < FELDER; column++) {
                    switch (field[row][column]){
                        case EMPTY_VALUE:
                            b[row][column].setText(EMPTY_SYMBOL);
                            b[row][column].setEnabled(true);
                            break;
                        case PLAYER_VALUE:
                            b[row][column].setText(PLAYER_SYMBOL);
                            b[row][column].setEnabled(false);
                            break;
                        case AI_VALUE:
                            b[row][column].setText(AI_SYMBOL);
                            b[row][column].setEnabled(false);
                    }
                    //clearing the saved board after usage
                    field[row][column] = EMPTY_VALUE;
                }
            }
            if (hardmode) {
                mTicTacToeGame.setBoardHard(field);
            } else mTicTacToeGame.setBoardNormal(field);

            getField();
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Save the current board to the model/player
     */
    protected void onPause() {
        super.onPause();

        if (hardmode) {
            mTicTacToeGame.setBoardHard(board);
        }else mTicTacToeGame.setBoardNormal(board);

        // Save player to the model
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }


    /**
     * This method resets the Board to start a new Game. It changes the starting Player
     * addToLayout creates new button if it is the first game or enables all buttons and
     * removes the player and Ai symbol
     **/
    private void setGameBoard()
    {
        mCurrGameState = GameState.GAME_STATE_SETUP;
        gameOver = EMPTY_VALUE;


        //Button action when clicked
        View.OnClickListener myListener = getOnClickListener();

        //making FELDER Button and add them to Layout
        addToLayout(myListener);

        //Every Second Game AI starts the game
        if(!playerStarted) aiMakeTurn();
        turnAvailable = true;

        getField();
        mCurrGameState = GameState.GAME_STATE_WAITING;
    }

    /**
     * The OnClickListener ist implemented here
     * Each button of the board gets the same Listener
     **/
    @NonNull
    private View.OnClickListener getOnClickListener() {
        return v -> {
            Button b = findViewById(v.getId());
            if (b.isEnabled() && turnAvailable)
            {
                b.setText(PLAYER_SYMBOL);
                b.setEnabled(false);
                if (lookForWinner(PLAYER_VALUE, board))
                {
                    dialogWinLostDraw(gameOver);
                }else aiMakeTurn();
            } else if (!turnAvailable) lookForWinner(PLAYER_VALUE, board); lookForWinner(AI_VALUE, board);

        };
    }

    /**
     * creating amount of #FELDER Buttons and give them an OnClickListener
     * while sorting them to a nice board
     **/
    @SuppressLint("InflateParams")
    private void addToLayout(View.OnClickListener myListener) {
        int counter = 0;

        for(int row = 0; row < FELDER; row ++)
        {
            for(int column = 0; column < FELDER; column++)
            {
                if (b[row][column] != null)
                {
                    b[row][column].setText(EMPTY_SYMBOL);
                    b[row][column].setEnabled(true);
                } else {
                    GridLayout.LayoutParams gridParam;

                    gridParam = new GridLayout.LayoutParams(
                            GridLayout.spec(counter % FELDER, GridLayout.CENTER, 1f),
                            GridLayout.spec(counter / FELDER, GridLayout.CENTER, 1f));
                    gridParam.setGravity(Gravity.FILL);


                    styleBoard(row, column, gridParam);

                    b[row][column] = (Button) LayoutInflater.from(this).inflate(R.layout.button_tictactoe, null, false);

                    b[row][column].setOnClickListener(myListener);
                    b[row][column].setId(counter);
                    b[row][column].setText(EMPTY_SYMBOL);
                    b[row][column].setEnabled(true);

                    gridLayout.addView(b[row][column], gridParam);
                }
                counter++;

            }

        }
    }

    /**
     *  Creating the boarders between the buttons to create a better looking TicTacToe board
     **/
    private void styleBoard(int row, int column, GridLayout.LayoutParams gridParam) {
        if (row == 0 && column != 0 && column != FELDER-1) gridParam.setMargins(0, BORD_MARGIN, 0, BORD_MARGIN); //top mid
        if (row == FELDER -1 && column != 0 && column != FELDER-1) gridParam.setMargins(0, BORD_MARGIN, 0, BORD_MARGIN); //botton mid
        if (column == 0 && row != 0 && row != FELDER-1) gridParam.setMargins(BORD_MARGIN, 0, BORD_MARGIN, 0); //left mid
        if (column == FELDER -1 && row != 0 && row != FELDER-1) gridParam.setMargins(BORD_MARGIN, 0, BORD_MARGIN, 0); //right mid

        if ((row != 0 && row != FELDER-1) && (column != 0 && column != FELDER-1)) {
            gridParam.setMargins(BORD_MARGIN, BORD_MARGIN, BORD_MARGIN, BORD_MARGIN);
        }
    }

    /**
     * The method looks for a winner before the Ai move and after.
     * The minimax Algorithm starts here and selects the field to press
     **/
    private void aiMakeTurn()
    {
        mCurrGameState = GameState.GAME_STATE_AI_TURN;
        getField();
        if (lookForWinner(PLAYER_VALUE, board))
        {
            mCurrGameState = GameState.GAME_STATE_GAMEOVER;
            dialogWinLostDraw(gameOver);
            return;
        }

        int [] buttonToPress;
        int row;
        int column;


        buttonToPress =  minimax(MINIMAX_DEPTH, AI_VALUE);
        row = buttonToPress[1];
        column = buttonToPress[2];


        if (b[row][column].isEnabled())
        {
            b[row][column].setText(AI_SYMBOL);
            b[row][column].setEnabled(false);
        }

        getField();
        if (lookForWinner(AI_VALUE, board)) {
            mCurrGameState = GameState.GAME_STATE_GAMEOVER;
            dialogWinLostDraw(gameOver);
        }
        mCurrGameState = GameState.GAME_STATE_WAITING;
    }


    /**
     * A recursive method to simulate moves of both player
     * The next move of the Ai is based on a heuristic evaluation, which
     * minimize the player and maximize the Ai
     * @return bestscore is the highest/lowest score when pressing field in bestrow/bestcolumn
     *
     **/
    private int[] minimax(int depth, int player) {
        // Generate possible next moves in a List of int[2] of {row, col}.
        List<int[]> nextMoves = getPossibleMoves();

        // mySeed is maximizing; while oppSeed is minimizing
        int bestScore = (player == AI_VALUE) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        int currentScore;
        int bestRow = -1;
        int bestCol = -1;

        if (nextMoves.isEmpty() || depth == 0) {
            // Gameover or depth reached, evaluate score
            bestScore = evaluate();
        } else {
            for (int[] move : nextMoves) {
                // Try this move for the current "player"
                board[move[0]][move[1]] = player;
                if (player == AI_VALUE) {  // mySeed (computer) is maximizing player
                    currentScore = minimax(depth - 1, PLAYER_VALUE)[0];
                    if (currentScore > bestScore) {
                        bestScore = currentScore;
                        bestRow = move[0];
                        bestCol = move[1];
                    }
                } else{
                    currentScore = minimax(depth - 1, AI_VALUE)[0];
                    if (currentScore < bestScore) {
                        bestScore = currentScore;
                        bestRow = move[0];
                        bestCol = move[1];
                    }
                }
                // Undo move
                board[move[0]][move[1]] = EMPTY_VALUE;

            }
        }
        return new int[] {bestScore, bestRow, bestCol};
    }


    /**
     * Evaluates each possible winning line of a 3x3 Board
     **/
    private int evaluate() {
        int score = 0;
        // Evaluate score for each of the 8 lines (3 rows, 3 columns, 2 diagonals)
        score += evaluateLine(0, 0, 0, 1, 0, 2);  // row 0
        score += evaluateLine(1, 0, 1, 1, 1, 2);  // row 1
        score += evaluateLine(2, 0, 2, 1, 2, 2);  // row 2
        score += evaluateLine(0, 0, 1, 0, 2, 0);  // col 0
        score += evaluateLine(0, 1, 1, 1, 2, 1);  // col 1
        score += evaluateLine(0, 2, 1, 2, 2, 2);  // col 2
        score += evaluateLine(0, 0, 1, 1, 2, 2);  // diagonal
        score += evaluateLine(0, 2, 1, 1, 2, 0);  // alternate diagonal
        return score;
    }

    /**
     * The heuristic evaluation function for the given line of 3 fields
     * @return +100, +10, +1 for 3-, 2-, 1-in-a-line for computer.
     * -100, -10, -1 for 3-, 2-, 1-in-a-line for opponent.
     * 0 otherwise
     * */
    private int evaluateLine(int row1, int col1, int row2, int col2, int row3, int col3) {
        int score = 0;

        // First field
        if (board[row1][col1] == AI_VALUE) {
            score = 1;
        } else if (board[row1][col1] == PLAYER_VALUE) {
            score = -1;
        }

        // Second field
        if (board[row2][col2] == AI_VALUE) {
            if (score == 1) {   // field 1 is AI_VALUE
                score = 10;
            } else if (score == -1) {  // field 1 is PLAYER_VALUE
                return 0;
            } else {  // cell1 is empty
                score = 1;
            }
        } else if (board[row2][col2] == PLAYER_VALUE) {
            if (score == -1) { // field 1 is oppSeed
                score = -10;
            } else if (score == 1) { // field 1 is AI_VALUE
                return 0;
            } else {  // cell1 is empty
                score = -1;
            }
        }

        // Third field
        if (board[row3][col3] == AI_VALUE) {
            if (score > 0) {  // field 1 and/or field 2 is AI_VALUE
                score *= 10;
            } else if (score < 0) {  // field 1 and/or field 2 is PLAYER_VALUE
                return 0;
            } else {  // field 1 and field 2 are empty
                score = 1;
            }
        } else if (board[row3][col3] == PLAYER_VALUE) {
            if (score < 0) {  // field 1 and/or field 2 is PLAYER_VALUE
                score *= 10;
            } else if (score > 1) {  // field 1 and/or field 2 is AI_VALUE
                return 0;
            } else {  // field 1 and field 2 are empty
                score = -1;
            }
        }
        return score;
    }


    /**
     * Searching empty fields on the board
     * @return list of all possible moves on the board or empty list, if no one can make move anymore
     **/
    private List<int[]> getPossibleMoves() {

        List<int[]> nextMoves = new ArrayList<>(); // allocate List

        // If gameover then no next move
        if (lookForWinner(AI_VALUE, board) || lookForWinner(PLAYER_VALUE, board)) {
            return nextMoves;   // return empty list
        }

        // Search for empty fields and add to the List
        for(int reihe = 0; reihe < FELDER; reihe ++)
        {
            for(int spalte = 0; spalte < FELDER; spalte++)
            {
                if (board[reihe][spalte] == EMPTY_VALUE) {
                    nextMoves.add(new int[] {reihe, spalte});
                }
            }
        }
        return nextMoves;
    }


    /**
     * dividing the virtual board (using int) and the board of buttons
     **/
    private void getField()
    {
        for(int reihe = 0; reihe < FELDER; reihe ++)
        {
            for(int spalte = 0; spalte < FELDER; spalte++)
            {
                if(b[reihe][spalte].getText().equals(EMPTY_SYMBOL))
                {
                    board[reihe][spalte] = EMPTY_VALUE;
                }else if(b[reihe][spalte].getText().equals(PLAYER_SYMBOL))
                {
                    board[reihe][spalte] = PLAYER_VALUE;
                }else if(b[reihe][spalte].getText().equals(AI_SYMBOL))
                {
                    board[reihe][spalte] = AI_VALUE;
                }

            }
        }
    }


    /**
     * Looks for the given Player, whether he won or it´s a draw
     * @param player the player which should be checked for
     * @param field the board in which it should be checked
     * @return true if won, otherwise false
     */
    private boolean lookForWinner(int player, int[][] field) {

        // looking for rows and columns
        for (int rowColoumn = 0; rowColoumn < FELDER; rowColoumn ++)
        {
            if(rowWinner(player, rowColoumn, field) || columnWinner(player, rowColoumn, field))
            {
                gameOver = player;
                return true;
            }
        }

        //looking for both diagonal
        if(diagonalWinnerRightLeft(player, field) || diagonalWinnerLeftRight(player, field))
        {
            gameOver = player;
            return true;
        }

        //looking for empty fields
        if(!lookForEmptyField(board))
        {
            gameOver = DRAW;
            return true;
        }


        return false;
    }

    /**
     * Checking for both Player.
     * @param field the board in which it should be checked
     * @return true if a winning state is on the board, false otherwise
     */
    private boolean lookForWinner(int[][] field) {
        return lookForWinner(PLAYER_VALUE, field) || lookForWinner(AI_VALUE, field);

    }

    /**
     * The Method check, whether the board has an empty field
     * @param field is a board[][] which will be checked
     * @return true if there is at least one empty field, returns false otherwise.
     */
    private boolean lookForEmptyField(int[][] field) {

        for(int reihe = 0; reihe < FELDER; reihe ++)
        {
            for(int spalte = 0; spalte < FELDER; spalte++)
            {
                if (field[reihe][spalte] == EMPTY_VALUE) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * The method check, whether the board has used fields
     * @param field is a board[][] which will be checked
     * @return true if there is at least one used field or if field = null, returns false otherwise.
     */
    private boolean lookForUsedField(int[][] field) {
        if (field != null) {
            for (int row = 0; row < FELDER; row++) {
                for (int column = 0; column < FELDER; column++) {
                    if (field[row][column] != EMPTY_VALUE) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * search for a winning state in diagonal starting in the right up corner going down to left
     * it is possible to expand from 3x3 Board to as big as wanted
     **/
    private boolean diagonalWinnerRightLeft(int player, int[][] field) {

        boolean[] diagonalCheck = new boolean[FELDER];

        for (int i = 0; i < FELDER; i++)
        {
            if (field[i][(FELDER-1) - i] == player) diagonalCheck[i] = true;
        }


        for (int i = 0; i < FELDER; i++)
        {
            if (!diagonalCheck[i])
            {
                return false;
            }
        }

        return true;
    }

    /**
     * search for a winning state in diagonal starting in the left up corner going down to right
     * it is possible to expand from 3x3 Board to as big as wanted
     **/
    private boolean diagonalWinnerLeftRight(int player, int[][] field) {

        boolean[] diagonalCheck = new boolean[FELDER];


        for (int i = 0; i < FELDER; i++)
        {
            if (field[i][i] == player) diagonalCheck[i] = true;
        }


        for (int i = 0; i < FELDER; i++)
        {
            if (!diagonalCheck[i])
            {
                return false;
            }
        }

        return true;
    }

    /**
     * search for a winning state in the given column
     * it is possible to expand from 3x3 Board to as big as wanted
     **/
    private boolean columnWinner(int player, int column, int[][] field) {

        boolean[] columnCheck = new boolean[FELDER];


        for (int i = 0; i < FELDER; i++)
        {
            if (field[i][column] == player) columnCheck[i] = true;
        }
        for (int i = 0; i < FELDER; i++)
        {
            if (!columnCheck[i])
            {
                return false;
            }
        }
        return true;
    }

    /**
     * search for a winning state in the given row
     * it is possible to expand from 3x3 Board to as big as wanted
     **/
    private boolean rowWinner(int player, int row, int field[][]) {

        boolean[] rowCheck = new boolean[FELDER];

        for (int i = 0; i < FELDER; i++)
        {
            if (field[row][i] == player) rowCheck[i] = true;
        }
        for (int i = 0; i < FELDER; i++)
        {
            if (!rowCheck[i])
            {
                return false;
            }
        }
        return true;
    }


    /**
     * if a gameover state is reached (no moves to go or one is winning) trigger the Alertdialog
     **/
    private void dialogWinLostDraw(int gameOver)
    {
        if(mCurrGameState == GameState.GAME_STATE_GAMEOVER) {
            turnAvailable = false;
            playerStarted = !playerStarted;

            switch (gameOver) {

                case PLAYER_VALUE:
                    // update the stats
                    mTicTacToeGame.won();

                    GameBoxUtils.showAlertDialog(this, R.string.game_title_tictactoe, R.string.dialog_message_won, this::setGameBoard, this::finish, this::finish);
                    break;
                case AI_VALUE:
                    // update the stats
                    mTicTacToeGame.lost();

                    GameBoxUtils.showAlertDialog(this, R.string.game_title_tictactoe, R.string.dialog_message_lost, this::setGameBoard, this::finish, this::finish);
                    break;

                case DRAW: //draw
                    // update the stats
                    mTicTacToeGame.draw();

                    GameBoxUtils.showAlertDialog(this, R.string.game_title_tictactoe, R.string.dialog_message_draw, this::setGameBoard, this::finish, this::finish);
                    break;
            }
        }

    }

    private enum GameState {
        GAME_STATE_WAITING,
        GAME_STATE_AI_TURN,
        GAME_STATE_GAMEOVER,
        GAME_STATE_SETUP
    }
}