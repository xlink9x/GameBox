package de.uniks.pmg.android5.gamebox;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import de.uniks.pmg.android5.gamebox.model.ColorGame;
import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class ColorGameActivity extends AppCompatActivity {

    /** How much a random color component (r, g, b) can be different from mMainColor. */
    private static final int COLOR_DEVIATION = 30;
    /** How much a random color needs to be different from mMainColor. */
    private static final int DEVIATION_LIMIT = 50;
    /** Maximum number of retries if random color is lower than the DEVIATION_LIMIT. */
    private static final int MAX_COLOR_GENERATION_RETRIES = 10;

    private Player mPlayer;
    private ColorGame mColorGame;

    // Views
    private ImageView mImageViewMain;
    private Button mButtonStartGame;
    private GridLayout mGridLayout;

    /** Main Color */
    private int mMainColor;

    private GameState mCurrentGameState;

    private Handler mHandler;

    /**
     * The {@link Runnable} that will set the visibility to INVISIBLE and after the given
     * time starts the mRunnableShowColors.
     */
    private Runnable mRunnableShowNothing = new Runnable() {
        @Override
        public void run() {
            // Don't show the main ImageView anymore
            mImageViewMain.setVisibility(View.INVISIBLE);

            // Show the colors after the given time
            mHandler.postDelayed(mRunnableShowColors, mColorGame.getDuration());
        }
    };

    /**
     * The {@link Runnable} that shows the ImageViews with the colors, one of them being the
     * mMainColor.
     */
    private Runnable mRunnableShowColors = new Runnable() {
        @Override
        public void run() {
            // Create new random colors
            List<Integer> colors = new LinkedList<>();
            for(int i = 0; i < mColorGame.getColors() - 1; i++) {
                colors.add(createRandomColor(true));
            }

            // Insert mMainColor at a random index to colors
            Random random = new Random();
            int randomIndex = random.nextInt(mColorGame.getColors());
            colors.add(randomIndex, mMainColor);

            // Layout GridLayout
            mGridLayout.setVisibility(View.VISIBLE);

            // Add ImageViews with the colors to the gridlayout
            for(int i = 0; i < colors.size(); i++) {
                // Create the ImageView that holds the color
                ImageView iv = new ImageView(ColorGameActivity.this);
                iv.setBackgroundColor(colors.get(i));

                // Set the Views tag to the corresponding color
                iv.setTag(colors.get(i));
                iv.setOnClickListener(mImageViewOnClickListener);

                // layout the ImageView within the mGridLayout, so that they are all the same size.
                GridLayout.LayoutParams params = new GridLayout.LayoutParams(
                        GridLayout.spec(GridLayout.UNDEFINED, GridLayout.CENTER, 1f),
                        GridLayout.spec(GridLayout.UNDEFINED, GridLayout.CENTER, 1f));
                params.setGravity(Gravity.FILL);
                iv.setLayoutParams(params);

                // Add the ImageView to the mGridLayout
                mGridLayout.addView(iv);
            }
        }
    };

    /**
     * {@link android.view.View.OnClickListener} that is used for the ImageViews that the user
     * needs to click.
     */
    private View.OnClickListener mImageViewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Set the current GameState to STATE_ENDED
            mCurrentGameState = GameState.STATE_ENDED;

            // Get the tag of the View, which is a color int
            int color = (int) v.getTag();

            if(color == mMainColor) {
                // User won
                mColorGame.won();

                // Show an AlertDialog and ask user if he wants to play again.
                GameBoxUtils.showAlertDialog(ColorGameActivity.this,
                        R.string.game_title_color, R.string.dialog_message_won,
                        ColorGameActivity.this::startGame, ColorGameActivity.this::finish,
                        ColorGameActivity.this::finish);

            } else {
                // User lost
                mColorGame.lost();

                // Show an AlertDialog and ask user if he wants to play again.
                GameBoxUtils.showAlertDialog(ColorGameActivity.this,
                        R.string.game_title_color, R.string.dialog_message_lost,
                        ColorGameActivity.this::startGame, ColorGameActivity.this::finish,
                        ColorGameActivity.this::finish);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_game);
        getSupportActionBar().setTitle(R.string.game_title_color);

        mPlayer = (Player)getIntent().getSerializableExtra("PLAYER");
        mColorGame = mPlayer.getColorGame();

        // Views
        mImageViewMain = findViewById(R.id.imageview_color_main);
        mButtonStartGame = findViewById(R.id.button_color_startgame);
        mGridLayout = findViewById(R.id.gridlayout_colors);

        mHandler = new Handler();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Remove callbacks from Handler
        mHandler.removeCallbacks(mRunnableShowNothing);
        mHandler.removeCallbacks(mRunnableShowColors);

        // If user leaves the activity while still playing, count it as a loss
        if(mCurrentGameState == GameState.STATE_PLAYING) {
            mColorGame.lost();
        }

        // Save the model
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }

    public void onStartGame(View view) {
        startGame();
    }

    /**
     * Starts the game by disabling the start button, removing all the old ImageViews,
     * creating a random color and calling the mRunnableShowNothing to initiate the creation
     * of the other ImageViews.
     */
    private void startGame() {
        // Set the current GameState to STATE_PLAYING
        mCurrentGameState = GameState.STATE_PLAYING;

        // Disable button
        mButtonStartGame.setVisibility(View.GONE);

        // Reset contents of GridView
        mGridLayout.removeAllViews();

        // Create main color
        mMainColor = createRandomColor(false);
        mImageViewMain.setBackgroundColor(mMainColor);
        mImageViewMain.setVisibility(View.VISIBLE);

        // Show nothing after the given time
        mHandler.postDelayed(mRunnableShowNothing, mColorGame.getDuration());
    }

    /**
     * Creates a random {@link android.support.annotation.ColorInt} and returns it. If the
     * deriveFromMainColor flag is true, it tries to create a color similar to the mMainColor.
     * @param deriveFromMainColor Flag that indicates whether or not the random color is similar to
     *                            mMainColor.
     * @return A {@link android.support.annotation.ColorInt} of a random color.
     */
    private int createRandomColor(boolean deriveFromMainColor) {
        Random random = new Random();
        int r, g, b;

        if(deriveFromMainColor) {
            // Get the components of mMainColor
            int R = (mMainColor >> 16) & 0xff;
            int G = (mMainColor >>  8) & 0xff;
            int B = (mMainColor      ) & 0xff;

            /*
            Create new color similar to main color
            This is done by creating random numbers for each of the color components (red, green,
            blue) between -COLOR_DEVIATION and COLOR_DEVIATION and adding them to the original
            color.
            We do this as often as the sum of the deviation values hit DEVIATION_LIMIT, but not
            more often than MAX_COLOR_GENERATION_RETRIES, since we need to wait for this method
            to return to continue the game. This whole process is done to make sure the derived
            color is distinguishable from the mMainColor.
            */

            int rDeviation = 0;
            int gDeviation = 0;
            int bDeviation = 0;

            int i = 0;
            while(i < MAX_COLOR_GENERATION_RETRIES &&
                    (Math.abs(rDeviation) + Math.abs(gDeviation) + Math.abs(bDeviation)) < DEVIATION_LIMIT ) {
                rDeviation = random.nextInt(2 * COLOR_DEVIATION) - COLOR_DEVIATION;
                gDeviation = random.nextInt(2 * COLOR_DEVIATION) - COLOR_DEVIATION;
                bDeviation = random.nextInt(2 * COLOR_DEVIATION) - COLOR_DEVIATION;
                ++i;
            }

            // Add deviation values to the original color
            r = R + rDeviation;
            g = G + gDeviation;
            b = B + bDeviation;

        } else {
            // Create new random color. The color will be a color between COLOR_DEVIATION and
            // 255 - COLOR_DEVIATION to avoid special cases where the color is black or white and
            // random derived colors are not distinguishable.

            r = random.nextInt(256 - 2 * COLOR_DEVIATION) + COLOR_DEVIATION;
            g = random.nextInt(256 - 2 * COLOR_DEVIATION) + COLOR_DEVIATION;
            b = random.nextInt(256 - 2 * COLOR_DEVIATION) + COLOR_DEVIATION;

        }
        return Color.rgb(r, g, b);
    }

    /**
     * The states for this game.<br>
     * <b>STATE_PLAYING</b>: The user is playing the game. We enter this state when the user presses
     * the startgame-button or restarts the game.<br>
     * <b>STATE_ENDED</b>: The game has ended naturally by either winning or losing the game.<br>
     */
    private enum GameState {
        STATE_PLAYING, STATE_ENDED
    }
}
