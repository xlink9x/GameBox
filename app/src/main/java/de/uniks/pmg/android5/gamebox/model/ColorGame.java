package de.uniks.pmg.android5.gamebox.model;

/**
 * Created by link9 on 21.12.17.
 */

public class ColorGame extends Game {
    public static final String GAME_NAME = "color";

    private static final int DURATION_NORMAL = 2000;
    private static final int DURATION_HARD = 1500;
    private static final int AMOUNT_COLORS_NORMAL = 4;
    private static final int AMOUNT_COLORS_HARD = 6;

    /** Duration in ms */
    private int duration;
    private int colors;

    public ColorGame() {
        this(Difficulty.NORMAL);
    }

    public ColorGame(Difficulty difficulty) {
        super(difficulty);

        setDifficulty(difficulty);
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        super.setDifficulty(difficulty);

        switch (difficulty) {
            case NORMAL:
                duration = DURATION_NORMAL;
                colors = AMOUNT_COLORS_NORMAL;
                break;
            case HARD:
                duration = DURATION_HARD;
                colors = AMOUNT_COLORS_HARD;
        }
    }

    public int getColors() {
        return colors;
    }

    public void setColors(int colors) {
        this.colors = colors;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
