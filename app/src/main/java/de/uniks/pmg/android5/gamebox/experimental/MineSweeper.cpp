#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 4  //Information into a global struct?
#define MINES 2 //no more than fields, causing an infinite loop
#define SPAWN 0.2 //needs to be calculated

//-78 = block
//255 = blank
//  -> x
//|
//v y

double RndPercent();
void printBoard(int h, int w, char a[h][w]);
void initBoard(int h, int w, char a[h][w], char c);
void placeMines(int h, int w, char a[h][h], int m);
void generateNumbers(int h, int w, char a[h][w]);
void revealCascade(int x, int y, int h, int w, char a[h][w], char s[h][w]);
int reveal(int x, int y, int h, int w, char a[h][w], char s[h][w]);
int checkWin(int h, int w, char a[h][w], int m);

int main(int argc, char *argv[]) {
    srand(time(NULL));
    char display[SIZE][SIZE];
    char solution[SIZE][SIZE];
    initBoard(SIZE, SIZE, display, -78);
    initBoard(SIZE, SIZE, solution, 255);
    placeMines(SIZE, SIZE, solution, MINES);
    generateNumbers(SIZE, SIZE, solution);
    printBoard(SIZE, SIZE, display);
    int x = 1, y = 1;
    while (1) {
        scanf("%d %d", &x, &y);// 0 0 eingeben um zu beenden
        if (x == 0 || y == 0)
            return 0;
        if (reveal(x-1, y-1, SIZE, SIZE, display, solution) == 1) {
            printBoard(SIZE, SIZE, display);
            printf("Game Over.");
            return 0;
        }
        if(checkWin(SIZE, SIZE, display, MINES) == 1) {
            printf("You win.");
            return 0;
        }
        printBoard(SIZE, SIZE, display);
    }
    return 0;
}

int checkWin(int h, int w, char a[h][w], int m) {
    int counter = 0;
    for(int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (a[i][j] == -78) {
                counter++;
            }
            if (counter > m)
                return 0;
        }
    }
    if(counter == m)
        return 1;
    else
        return 0;
}

int reveal(int x, int y, int h, int w, char a[h][w], char s[h][w]) {
    if(a[y][x] == ' ') {
        return 0;
    } else if(s[y][x] == '*') {
        a[y][x] = '*';
        return 1;
    } else if(s[y][x] <= 57 && s[y][x] >= 49) {
        a[y][x] = s[y][x];
        return 0;
    } else {
        a[y][x] = ' ';
        for(int i = -1; i < 2; i++) {
            for(int j = -1; j < 2; j++) {
                if(!(i == 0 && j == 0) &&
                    x+j >= 0 && x+j < w &&
                    y+i >= 0 && y+i < h)
                        reveal(x+j, y+i, h, w, a, s);
            }
        }
        return 0;
    }
}

void generateNumbers(int h, int w, char a[h][w]) {
    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            if(a[i][j] != '*') {
                char counter = 0;
                for(int k = -1; k < 2; k++) {
                    for(int l = -1; l < 2; l++) {
                        if(i+k >= 0 && i+k <= h-1 && j+l >= 0 && j+l <= w-1) {
                            if(a[i+k][j+l] == '*')
                                counter++;
                        }
                    }
                }
                a[i][j] = counter==0?255:counter+48;
            }
        }
    }
}

void placeMines(int h, int w, char a[h][w], int m) {
    while (m != 0) {
        for(int i = 0; i < h; i++) {
            for(int j = 0; j < w; j++) {
                if(RndPercent() < SPAWN && a[i][j] != '*') {
                    a[i][j] = 42;
                    m--;
                }
                if(m==0)
                    return;
            }
        }
    }
}

void initBoard(int h, int w, char a[h][w], char c) {
    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            a[i][j] = c;
        }
    }
}

void printBoard(int h, int w, char a[h][w]) {
    printf("  ");
    for(int i = 0; i < w; i++) {
        printf("%d", i+1);
    }
    printf("\n");
    for(int i = 0; i < h; i++) {
        printf("%d ", i+1);
        for(int j = 0; j < w; j++) {
            printf("%c", a[i][j]);
        }
        printf("\n");
    }
}



double RndPercent() {
    return (double)rand() / (double)RAND_MAX ;
}
