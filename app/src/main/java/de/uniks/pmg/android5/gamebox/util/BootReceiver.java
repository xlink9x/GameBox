package de.uniks.pmg.android5.gamebox.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 *  This class makes sure that the gamebox alarm clock is still set after reboot
 */

public class BootReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            NotificationPublisher.setAlarm(context);
        }
    }
}
