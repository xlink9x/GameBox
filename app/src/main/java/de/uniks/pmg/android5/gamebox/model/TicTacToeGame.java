package de.uniks.pmg.android5.gamebox.model;

public class TicTacToeGame extends Game {
    private static final String GAME_NAME = "tictactoe";

    private int squares = 3;

    private int[][] boardNormal;
    private int[][] boardHard;


    private TicTacToeGame(Difficulty difficulty) {
        super(difficulty);
    }

    public TicTacToeGame() {
        this(Difficulty.NORMAL);
    }

    @Override
    public String getGameName() {
        return GAME_NAME;
    }

    public int getSquares() {
        return squares;
    }


    public void setBoardNormal(int[][] board) {
        this.boardNormal = board;
    }


    public int[][] getBoardNormal() {
        return boardNormal;
    }

    public void setBoardHard(int[][] board) {
        this.boardHard = board;
    }


    public int[][] getBoardHard() {
        return boardHard;
    }

    public void setSquares(int squares) {
        this.squares = squares;
    }
}
