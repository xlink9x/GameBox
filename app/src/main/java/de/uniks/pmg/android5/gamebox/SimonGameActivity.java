package de.uniks.pmg.android5.gamebox;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.SimonGame;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class SimonGameActivity extends AppCompatActivity {

    private static final String STATE_INDEX = "CurrentIndex";
    private static final String STATE_GAMESTATE = "CurrentGameState";
    private static final String STATE_PLAYER = "Player";
    private static final String STATE_GAME = "Game";

    private static final int AMOUNT_BUMPERS = 4;

    private static final int INIT_SEQUENCE_DELAY_MILLIS = 550;
    private static final int DELAY_MILLIS_FOCUS = 200;
    private static final int DELAY_MILLIS_UNFOCUS = 600;

    private static final int AUDIO_ID_BUMPER_1 = R.raw.g3;
    private static final int AUDIO_ID_BUMPER_2 = R.raw.e4;
    private static final int AUDIO_ID_BUMPER_3 = R.raw.c4;
    private static final int AUDIO_ID_BUMPER_4 = R.raw.g4;

    private Player mPlayer;
    private SimonGame mSimonGame;

    private GameState mCurrentGameState;

    /**
     * mCurrentIndex resembles how many inputs the user
     * has done in WAIT_FOR_USER_INPUT state.
     */
    private int mCurrentIndex;

    private ImageView[] mBumpers; /** Array of the bumpers */
    private TextView mTextviewCurrentLevel;
    private TextView mTextviewGoal;

    private Handler mSequenceHandler;
    /**
     * The Runnables that show the sequence. It will be handled (and called)
     * by the mSequenceHandler. When these are called, they will call themselves
     * until the sequence is over.
     */
    private Runnable mRunnableBumperFocus = new Runnable() {
        @Override
        public void run() {
            // Make the bumpers focusable
            setBumpersFocusable(true);

            // Get the value from the current index from sequence and focus the bumper
            int currentFromSequence = mSimonGame.getSequence().get(mCurrentIndex);
            mBumpers[currentFromSequence].requestFocus();
            playSound(mBumpers[currentFromSequence]);

            // Increase mCurrentIndex
            mCurrentIndex++;

            mSequenceHandler.postDelayed(mRunnableBumperUnfocus, mSimonGame.getTimeOn());
        }
    };

    private Runnable mRunnableBumperUnfocus = new Runnable() {
        @Override
        public void run() {
            // Unfocus all bumpers
            setBumpersFocusable(false);

            if(mCurrentIndex == mSimonGame.getSequenceLength()) {
                // finished the sequence, go into next state
                mCurrentIndex = 0;
                mCurrentGameState = GameState.STATE_WAIT_FOR_USER_INPUT;

                // Make bumpers clickable again
                for(ImageView iv : mBumpers) {
                    iv.setClickable(true);
                }
            } else {
                // sequence not finished, focus the next bumper
                mSequenceHandler.postDelayed(mRunnableBumperFocus, mSimonGame.getTimeOff());
            }

        }
    };

    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simon_game);
        getSupportActionBar().setTitle(R.string.game_title_simon);

        if(savedInstanceState != null) {
            // Restore values from Bundle saved in the onSaveInstanceState()
            mCurrentIndex = savedInstanceState.getInt(STATE_INDEX);
            mCurrentGameState = (GameState) savedInstanceState.getSerializable(STATE_GAMESTATE);
            mPlayer = (Player) savedInstanceState.getSerializable(STATE_PLAYER);
            mSimonGame = (SimonGame) savedInstanceState.getSerializable(STATE_GAME);
        } else {
            mPlayer = (Player)getIntent().getSerializableExtra("PLAYER");
            mSimonGame = mPlayer.getSimonGame();

            // Initial state of the game when it first starts.
            mCurrentGameState = GameState.STATE_CPU_SHOWS_SEQUENCE;
            mCurrentIndex = 0;
        }

        // Create the handler
        mSequenceHandler = new Handler();

        // Get all the ImageViews (bumpers) in the bumper array.
        mBumpers = new ImageView[AMOUNT_BUMPERS];
        mBumpers[0] = findViewById(R.id.bumper_simon_1);
        mBumpers[1] = findViewById(R.id.bumper_simon_2);
        mBumpers[2] = findViewById(R.id.bumper_simon_3);
        mBumpers[3] = findViewById(R.id.bumper_simon_4);

        mTextviewCurrentLevel = findViewById(R.id.textview_simon_level);
        mTextviewGoal = findViewById(R.id.textview_simon_goal);


        // start the game in the model
        mSimonGame.startGame();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        The reason why we start the sequence here is so that it will also show
        when the user paused and continued the app. To not make it so easy (user can
        always watch the entire sequence over and over again by just pausing and continuing
        the app), the sequence starts at current index, which will not reset after pausing.
         */

        updateUI();

        // Start the sequence here, if the user is in STATE_CPU_SHOWS_SEQUENCE state
        if(mCurrentGameState == GameState.STATE_CPU_SHOWS_SEQUENCE) {
            showSequence();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Save currentIndex and currentGameState to the bundle
        outState.putInt(STATE_INDEX, mCurrentIndex);
        outState.putSerializable(STATE_GAMESTATE, mCurrentGameState);
        outState.putSerializable(STATE_PLAYER, mPlayer);
        outState.putSerializable(STATE_GAME, mSimonGame);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Remove the Callbacks from Handler
        mSequenceHandler.removeCallbacks(mRunnableBumperFocus);
        mSequenceHandler.removeCallbacks(mRunnableBumperUnfocus);

        // Free MediaPlayer
        try {
            if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
            }
        } catch(IllegalStateException e) {
            // Do nothing.
        }

        // Count the current game as a loss, when the player is still playing
        if(mCurrentGameState != GameState.STATE_GAME_ENDED) {
            mSimonGame.lost();
        }

        // Save the model
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }

    /**
     * Restarts the game.
     */
    private void restartGame() {
        // Stop any existing Runnables (so that the 'animation' will stop)
        mSequenceHandler.removeCallbacks(mRunnableBumperFocus);
        mSequenceHandler.removeCallbacks(mRunnableBumperUnfocus);

        // Set the focusable values for all bumpers back to false.
        // We need this because it may happen that the 'wrong' Runnable
        // was killed and therefore it maybe never happened.
        setBumpersFocusable(false);

        // Restart the game in the model.
        mSimonGame.restartGame();

        // Change GameState back to STATE_CPU_SHOWS_SEQUENCE,
        // set mCurrentIndex back to 0 and show the sequence
        mCurrentGameState = GameState.STATE_CPU_SHOWS_SEQUENCE;
        mCurrentIndex = 0;

        // reset ui
        updateUI();

        showSequence();
    }

    /**
     * If it is the users turn (mCurrentGameState is set to WAIT_FOR_USER_INPUT), this method
     * can be called to make a turn, which means that the user did input (by clicking a bumper).
     *
     * This methods first checks if we are in the right GameState. If so, we check if the user
     * did click the right bumper according to the current index in the sequence from the
     * mSimonGame.
     * If he clicked the right bumper, the game will continue by appending a new
     * value to the sequence.
     * If did not click the right bumper, it will call {@link #onUserLost()}.
     * @param bumper The bumper that the user clicked (0-indexed)
     */
    private void makeTurn(int bumper) {
        // We can only make a turn if the game is in the WAIT_FOR_USER_INPUT state
        if(mCurrentGameState != GameState.STATE_WAIT_FOR_USER_INPUT) return;

        if(!mSimonGame.checkTurn(mCurrentIndex, bumper)) {
            // user has lost
            onUserLost();
            return;
        }

        mCurrentIndex++;
        if(mCurrentIndex == mSimonGame.getSequenceLength()) {
            // User got the complete sequence right.

            // Has the user won the game already?
            if(mCurrentIndex == mSimonGame.getAmountTurns() + mSimonGame.getAmountStartTones() - 1) {
                mTextviewCurrentLevel.setText("" + mSimonGame.getAmountTurns());
                onUserWon();
            } else {
                // Make Bumpers not clickable anymore
                for(ImageView iv : mBumpers) {
                    iv.setClickable(false);
                }

                // extend the sequence
                mSimonGame.extendSequence();

                mCurrentIndex = 0;
                mCurrentGameState = GameState.STATE_CPU_SHOWS_SEQUENCE;

                updateUI();

                showSequence();
            }
        }
    }

    /**
     * Updates all the UI elements to show updated information about the game.
     */
    private void updateUI() {
        // Update level and goal textview
        mTextviewCurrentLevel.setText(
                "" + (mSimonGame.getSequenceLength() - mSimonGame.getAmountStartTones()));

        mTextviewGoal.setText("" + mSimonGame.getAmountTurns());
    }

    /**
     * Shows the sequence randomly generated by the mSimonGame object. It uses the
     * {@link Handler} mSequenceHandler and the two {@link Runnable}s mRunnableBumperFocus
     * and mRunnableBumperUnfocus to sequentially focus and unfocus the appropriate bumpers
     * (ImageViews) while using some delays.
     */
    private void showSequence() {
        mSequenceHandler.postDelayed(mRunnableBumperFocus, INIT_SEQUENCE_DELAY_MILLIS);
    }

    /**
     * Sets all the bumpers (ImageViews) focusable value to flag.
     * @param flag Whether or not the bumpers should be focusable.
     */
    private void setBumpersFocusable(boolean flag) {
        for (ImageView iv : mBumpers) {
            iv.setFocusable(flag);
            iv.setFocusableInTouchMode(flag);
            if(!flag) iv.clearFocus();
        }
    }

    /**
     * Call this if the user has lost the game.
     */
    private void onUserLost() {
        // change the GameState
        mCurrentGameState = GameState.STATE_GAME_ENDED;

        // Let the model handle the lost
        mSimonGame.lost();

        // Show a dialog and ask user to retry
        GameBoxUtils.showAlertDialog(this, R.string.game_title_simon,
                R.string.dialog_message_lost, this::restartGame, this::finish, this::finish);
    }

    /**
     * Call this if the user has won the game
     */
    private void onUserWon() {
        // change the GameState
        mCurrentGameState = GameState.STATE_GAME_ENDED;

        // Let the model handle the win
        mSimonGame.won();

        // Show a dialog and ask user to retry
        GameBoxUtils.showAlertDialog(this, R.string.game_title_simon,
                R.string.dialog_message_won, this::restartGame, this::finish, this::finish);
    }

    /**
     * Plays the sound for the given view. The view must be one of the bumpers,
     * otherwise there will be no sound.
     * @param view
     */
    private void playSound(View view) {
        try {
            if(mMediaPlayer == null) {
                // Setup the MediaPlayer on first start
                mMediaPlayer = MediaPlayer.create(this, getAudioId(view));
            }
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = MediaPlayer.create(this, getAudioId(view));
            }
            mMediaPlayer.start();
        } catch(Exception e) {
            // DO NOTHING!
        }
    }

    /**
     * Retrieves the sound id for the given view. This view must be one of the bumpers,
     * otherwise this method will return 0.
     * This will give you the ResourceId. To change the ResourceId for the bumpers,
     * change the constants {@link #AUDIO_ID_BUMPER_1}, {@link #AUDIO_ID_BUMPER_2},
     * {@link #AUDIO_ID_BUMPER_3} or {@link #AUDIO_ID_BUMPER_4}.
     * @param view The {@View} object to retrieve the sound id from. Must be one of
     *             the bumpers.
     * @return The ResourceId of the sound file, or 0 if it gets an unknown {@link View}.
     */
    private int getAudioId(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.bumper_simon_1:
                return AUDIO_ID_BUMPER_1;
            case R.id.bumper_simon_2:
                return AUDIO_ID_BUMPER_2;
            case R.id.bumper_simon_3:
                return AUDIO_ID_BUMPER_3;
            case R.id.bumper_simon_4:
                return AUDIO_ID_BUMPER_4;
        }
        return 0;
    }

    /**
     * This method gets called when one of the bumpers in the ui is clicked.
     * We check which bumper was clicked and call makeTurn().
     * @param view Required {@link View} object for android. It contains the
     *             {@link View} that called this method.
     */
    public void onBumperClicked(View view) {
        int id = view.getId();

        playSound(view);

        switch(id) {
            case R.id.bumper_simon_1:
                makeTurn(0);
                break;
            case R.id.bumper_simon_2:
                makeTurn(1);
                break;
            case R.id.bumper_simon_3:
                makeTurn(2);
                break;
            case R.id.bumper_simon_4:
                makeTurn(3);
                break;
        }

    }

    /**
     * The states for this game.<br>
     * <b>STATE_WAIT_FOR_USER_INPUT</b>: The game waits for the user to input the sequence. We will
     * be in this state as long as the user needs to input it or until he does something wrong.<br>
     * <b>STATE_CPU_SHOWS_SEQUENCE</b>: The game will show the sequence to the user. In this state
     * all user interactions are disabled (clicking any bumper will do nothing). We are in this
     * state until the sequence was completely shown. After that we automatically switch back to the
     * STATE_WAIT_FOR_USER_INPUT.<br>
     * <b>STATE_GAME_ENDED</b>: The user has gotten the complete sequence right or did something
     * wrong. This state lasts as long as the user decides to leave (not retrying the game) or he
     * wants to try again. In this case, we switch back to STATE_CPU_SHOWS_SEQUENCE.<br>
     */
    private enum GameState {
        STATE_WAIT_FOR_USER_INPUT,
        STATE_CPU_SHOWS_SEQUENCE,
        STATE_GAME_ENDED
    }
}
