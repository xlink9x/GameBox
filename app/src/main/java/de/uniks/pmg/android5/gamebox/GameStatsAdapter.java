package de.uniks.pmg.android5.gamebox;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.uniks.pmg.android5.gamebox.model.Game;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class GameStatsAdapter extends RecyclerView.Adapter<GameStatsAdapter.ViewHolder> {
    private List<Game> mDataset;
    private Game.Difficulty difficulty;

    public GameStatsAdapter(List<Game> data, Game.Difficulty diff) {
        mDataset = data;
        difficulty = diff;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.statistics_recycler_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Game game = mDataset.get(position);

        // Load the right Image to the ImageView
        holder.mImageViewGame.setImageResource(GameBoxUtils.getImagePreviewForGame(game));

        int wins = 0;
        int loses = 0;
        int plays = 0;

        // Get the stats by the given difficulty
        switch (difficulty) {
            case NORMAL:
                wins = game.getStatsNormal()[0];
                loses = game.getStatsNormal()[1];
                plays = game.getStatsNormal()[2];
                break;
            case HARD:
                wins = game.getStatsHard()[0];
                loses = game.getStatsHard()[1];
                plays = game.getStatsHard()[2];
                break;
        }

        // Get the Games name
        String name = GameBoxUtils.getGameTitleForGame(holder.itemView.getContext(), game);
        holder.mTextViewGameName.setText(name);

        holder.mTextViewAmountWon.setText(wins+"");
        holder.mTextViewAmountLost.setText(loses+"");

        // Calculate the draws as follows: draws = played - (won + lost)
        int draws = plays - (wins + loses);
        holder.mTextViewAmountDraw.setText(draws+"");

        double winRate = game.getWinRate(difficulty);
        holder.mTextViewWinRate.setText(String.format("%.2f%%", winRate*100));

        // Set the background color alternating
        if(position % 2 == 0) {
            holder.itemView.setBackgroundResource(R.color.lightGrey);
        } else {
            holder.itemView.setBackgroundResource(R.color.lighterGrey);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageViewGame;
        public TextView mTextViewGameName;
        public TextView mTextViewAmountWon;
        public TextView mTextViewAmountLost;
        public TextView mTextViewAmountDraw;
        public TextView mTextViewWinRate;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageViewGame = itemView.findViewById(R.id.imageview_statistics_game);
            mTextViewGameName = itemView.findViewById(R.id.textview_statistics_gamename);
            mTextViewAmountWon = itemView.findViewById(R.id.textview_statistics_amount_won);
            mTextViewAmountLost = itemView.findViewById(R.id.textview_statistics_amount_lost);
            mTextViewAmountDraw = itemView.findViewById(R.id.textview_statistics_amount_draw);
            mTextViewWinRate = itemView.findViewById(R.id.textview_statistics_winrate);
        }
    }
}
