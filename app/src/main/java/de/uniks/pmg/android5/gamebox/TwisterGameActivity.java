package de.uniks.pmg.android5.gamebox;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;
import java.util.stream.IntStream;

import de.uniks.pmg.android5.gamebox.model.Player;
import de.uniks.pmg.android5.gamebox.model.TwisterGame;
import de.uniks.pmg.android5.gamebox.util.GameBoxUtils;

public class TwisterGameActivity extends AppCompatActivity implements SensorEventListener {

    private static final int DEFAULT_COUNTDOWN_TIME = 2;
    /** The deviation that is used to check whether we hit a certain checkpoint or not, in degree */
    private static final int ANGLE_DEVIATION = 25;
    private static final int SENSOR_DELAY = SensorManager.SENSOR_DELAY_GAME;
    private static final int GAME_OVER_SOUND_ID = R.raw.c4;

    // Model objects
    private Player mPlayer;
    private TwisterGame mTwisterGame;

    // Sensors
    private Sensor mSensor;
    private SensorManager mSensorManager;

    // Views
    private TextView mTextViewCountdown;
    private TextView mTextViewTurns;
    private Button mButtonStart;
    private ConstraintLayout mConstrainLayout;

    // The MediaPlayer to play the sound when the game is over
    private MediaPlayer mMediaPlayer;

    // Current GameState of the game
    private GameState mCurrentGameState;

    // Amount of made turns
    private int mAmountTurns;

    // Axis
    private float mStartAxis = 0;

    // The direction the player turns
    private Direction mTurningDirection;

    private boolean mFirstTurn;

    // Checkpoints for checking if a turn was made
    private int[] mCircleCheckpoints = new int[] {45, 135, 225, 305};
    private boolean [] mCheckpoints = new boolean[mCircleCheckpoints.length];

    // Flag to see if the game has started
    private boolean mGameStart;

    // Timers for the counters
    private int mCountUpTimer;
    private int mCountDownTimer;

    // Handles the Runnables (Countdown and Countup)
    private Handler mHandler = new Handler();

    /**
     * The Runnable that counts the time down until the game starts.
     */
    private Runnable mCountdownRunnable = new Runnable() {
        @Override
        public void run() {
            mTextViewCountdown.setText("" + mCountDownTimer);
            mCountDownTimer--;
            if (mCountDownTimer > -1) {
                countDown(1000);
            } else {
                startGame();
            }
        }
    };

    /**
     * The Runnable that counts the time up until the game ends.
     */
    private Runnable mCountUpRunnable = new Runnable() {
        @Override
        public void run() {
            mTextViewCountdown.setText("" + mCountUpTimer);
            mCountUpTimer++;
            if (mCountUpTimer <= mTwisterGame.getDuration()) {
                countUp(1000);
            } else {
                //mTextViewCountdown.setText("" + mTwisterGame.getDuration());
                endGame();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twister_game);
        getSupportActionBar().setTitle(R.string.game_title_twister);

        if(savedInstanceState != null) {
            // Restore values from Bundle saved in the onSaveInstanceState()
        } else {
            mPlayer = (Player)getIntent().getSerializableExtra("PLAYER");
            mTwisterGame = mPlayer.getTwisterGame();
            mTwisterGame.setDuration(10);

            mTextViewCountdown = findViewById(R.id.textView_Countdown);
            mTextViewTurns = findViewById(R.id.textView_Turns);
            mButtonStart = findViewById(R.id.button_twister_start);
            mConstrainLayout = findViewById(R.id.constraint_layout_twister);
        }

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        refresh();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);

        // Remove callbacks from handler
        mHandler.removeCallbacks(mCountdownRunnable);
        mHandler.removeCallbacks(mCountUpRunnable);

        // Free MediaPlayer
        try {
            if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
            }
        } catch(IllegalStateException e) {
            // Do nothing.
        }

        // If user leaves activity while game is running, count it as a loss
        if(mCurrentGameState == GameState.RUNNING) {
            mTwisterGame.lost();
        }

        // Save the model
        GameBoxUtils.savePlayerToJson(this, mPlayer);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {


        float[] rMatTemp = new float[9];
        float[] rVec = new float[3];
        float[] result = new float[3];

        for (int i = 0; i<3; i++) {
            rVec[i] = sensorEvent.values[i];
        }

        mSensorManager.getRotationMatrixFromVector(rMatTemp,rVec);
        mSensorManager.getOrientation(rMatTemp, result);

        GameBoxUtils.convertToDegrees(result);

        if (!mGameStart) {
            mGameStart = true;
            mStartAxis = result[0];
        }

        float angle;

        if(result[0]- mStartAxis < 0) {
            angle = 360 + result[0] - mStartAxis;
        } else {
            angle = result[0] - mStartAxis;
        }

        // check if we are in the right GameState
        if(mCurrentGameState == GameState.RUNNING) {
            checkForTurn(angle);
        }

    }

    /**
     * Sets all values to their default values.
     */
    private void refresh() {
        // Show the button
        mButtonStart.setVisibility(View.VISIBLE);

        // Reset TextViews
        mTextViewCountdown.setText("");
        mTextViewTurns.setText(R.string.textview_twister_turns);

        mConstrainLayout.setBackgroundResource(android.R.color.background_light);

        // Reset the mCheckpoints
        Arrays.fill(mCheckpoints, false);

        // Reset all fields
        mAmountTurns = 0;
        mFirstTurn = true;
        mGameStart = false;
        mTurningDirection = Direction.RIGHT;
        mCountDownTimer = DEFAULT_COUNTDOWN_TIME;
        mCountUpTimer = 1;

        // Change GameState to STARTING
        mCurrentGameState = GameState.STARTING;
    }

    /**
     * Checks if a turn was made. It will check if certain checkpoints has been reached on the way.
     * This method will take care of the turning direction, which can not be changed during the
     * game.
     * @param angle The angle in degrees.
     */
    private void checkForTurn(float angle) {

        /* After we reach the first checkpoint we can determine in which direction the user turns.
         * Either the first or last checkpoint has been reached then. This test must just be made
         * once, after the direction was detected it is not allowed to change the direction anymore.
         */

        if(mFirstTurn) {
            if(angle > mCircleCheckpoints[0] - ANGLE_DEVIATION && angle < mCircleCheckpoints[0] + ANGLE_DEVIATION) {
                mTurningDirection = Direction.RIGHT;
                mFirstTurn = false;
            } else if (angle > mCircleCheckpoints[mCircleCheckpoints.length - 1] - ANGLE_DEVIATION &&
                    angle < mCircleCheckpoints[mCircleCheckpoints.length - 1] + ANGLE_DEVIATION) {
                mTurningDirection = Direction.LEFT;
                mFirstTurn = false;
            }
        }


        // Detect left and right turns
        switch (mTurningDirection) {
            case RIGHT:
                for (int i = 0; i < mCheckpoints.length; i++ ) {
                    if(angle > mCircleCheckpoints[i] - ANGLE_DEVIATION && angle < mCircleCheckpoints[i] + ANGLE_DEVIATION) {
                        // check if all previous checkpoints has been reached
                        boolean allPrevCheckpoints = true;
                        for(int j = 0; j < i; j++) {
                            if(!mCheckpoints[j]) {
                                allPrevCheckpoints = false;
                                break;
                            }
                        }
                        if(allPrevCheckpoints) {
                            mCheckpoints[i] = true;
                        }
                    }
                }
                break;
            case LEFT:
                for(int i = mCheckpoints.length - 1; i >= 0; i--) {
                    if(angle > mCircleCheckpoints[i] - ANGLE_DEVIATION && angle < mCircleCheckpoints[i] + ANGLE_DEVIATION) {
                        // check if all previous checkpoints has been reached
                        boolean allPrevCheckpoints = true;
                        for(int j = mCheckpoints.length - 1; j > i; j--) {
                            if(!mCheckpoints[j]) {
                                allPrevCheckpoints = false;
                                break;
                            }
                        }
                        if(allPrevCheckpoints) {
                            mCheckpoints[i] = true;
                        }
                    }
                }
        }

        // If all mCheckpoints has been reached and we are back to 0, we have a full turn
        long count = IntStream.range(0, mCheckpoints.length)
                .mapToObj(i -> mCheckpoints[i])
                .filter(b -> b.equals(Boolean.FALSE))
                .count();

        if((angle > 360 - ANGLE_DEVIATION || angle < ANGLE_DEVIATION) && count == 0) {
            // Full turn
            onTurnCompleted();
        }

    }

    /**
     * Called when a full turn was made. It will update the counter for the amount of turns,
     * update the TextView and reset the checkpoints, so that a new turn can be detected.
     */
    private void onTurnCompleted() {
        // Increase amount of turns
        mAmountTurns++;

        // Update the textview for the turns
        mTextViewTurns.setText("" + mAmountTurns);

        // If you have enough turns made, also change the color
        if(mAmountTurns >= mTwisterGame.getTurns()) {
            // Change the background color
            mConstrainLayout.setBackgroundResource(R.color.twister_won);
        }

        // Reset the checkpoints
        Arrays.fill(mCheckpoints, false);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // DO NOTHING AT ALL!
    }

    public void onStartButtonClicked(View view) {
        countDown(200);
        view.setVisibility(View.GONE);
    }

    /**
     * Starts the Runnable {@link #mCountdownRunnable} with the given delay.
     * @param millis The delay until the Runnable gets called.
     */
    private void countDown(int millis) {
        mHandler.postDelayed(mCountdownRunnable, millis);
    }

    /**
     * Starts the Runnable {@link #mCountUpRunnable} with the given delay.
     * @param millis The delay until the Runnable gets called.
     */
    private void countUp(int millis) {
        mHandler.postDelayed(mCountUpRunnable, millis);
    }

    /**
     * Restarts the game.
     */
    private void restartGame() {
        refresh();
    }

    /**
     * Starts the game.
     */
    private void startGame() {
        mSensorManager.registerListener(this, mSensor, SENSOR_DELAY);

        // Change GameState
        mCurrentGameState = GameState.RUNNING;

        // Reset the amount of turns
        mAmountTurns = 0;

        // Change the countdown textviews text
        mTextViewCountdown.setText("GO");

        // Start counting up
        countUp(1000);
    }

    /**
     * Ends the game. It also unregisters the Sensor-Listener, so it needs to be reattached when you
     * want to start the game again. Plays a sound to indicate that the game is over.
     */
    private void endGame() {
        mSensorManager.unregisterListener(this);
        mGameStart = false;

        // Play a sound
        try {

            if(mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer.create(this, GAME_OVER_SOUND_ID);
            }
            if(mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = MediaPlayer.create(this, GAME_OVER_SOUND_ID);
            }
            mMediaPlayer.start();
        } catch (Exception e) {
            // EMPTY CATCH
        }

        mCurrentGameState = GameState.ENDING;
        mTextViewCountdown.setText(R.string.twister_message_end_game);

        // Check if user has won or lost and show retry message
        if(mAmountTurns >= mTwisterGame.getTurns()) {
            // User won
            mTwisterGame.won();
            GameBoxUtils.showAlertDialog(this, R.string.game_title_twister,
                    R.string.dialog_message_won, this::restartGame, this::finish, this::finish);
        } else {
            // User lost
            mTwisterGame.lost();
            GameBoxUtils.showAlertDialog(this, R.string.game_title_twister,
                    R.string.dialog_message_lost, this::restartGame, this::finish, this::finish);
        }

    }

    /**
     * The GameStates of this game.<br>
     * <b>Starting</b>: The Game is about to start. In this state the Countdown will count down to 0, the
     * player can adjust his stand and make ready. After the countdown hit 0, it switches to the
     * RUNNING-state.<br>
     * <b>Running</b>: The game is running, that means the counter is counting up until it reaches
     * the given duration. In this state the sensors get evaluated to detect full turns. After the
     * given duration it switches to the ENDING-state.<br>
     * <b>ENDING</b>: The game has ended. From this state you can switch back to the RUNNING-state.
     */
    private enum GameState {
        STARTING, RUNNING, ENDING
    }

    /**
     * Holds information about the direction the player is turning.
     */
    private enum Direction {
        LEFT, RIGHT
    }

}

